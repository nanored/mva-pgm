\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}
\usepackage{multirow}

\graphicspath{{figures/}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Modèles graphiques probabiliste --- DM 1
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont, Alaeddine Sabbagh}

\DeclareMathOperator{\com}{com}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\diag}{\text{\textbf{diag}}~}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\ind}[1]{\mathbbm{1}_{#1}}

\begin{document}
	
	\maketitle
	
	\section{Apprentissage dans les modèles graphiques discrets}
	
	On considère $n$ échantillons $(z_i, x_i)_{i = 1}^n$ identiquement distribués selon la loi de $(x, z)$. Puis on observe les $n$ valeurs $(m_i, k_i)_{i = 1}^n$. On veut alors maximiser la log-vraisemblance suivante~:
	$$ \begin{array}{lll}
	L_{m, k}(\pi, \theta) & = & \displaystyle \sum_{i=1}^n \log p \left( z_i = m_i, x_i = k_i \mid \pi, \theta \right) \\
	& = & \displaystyle \sum_{i=1}^n \log \left[ p(z_i = m | \pi) p(x_i = k_i | z_i = m_i, \theta) \right] \\
	& = & \displaystyle \sum_{i=1}^n \log \pi_{m_i} \, + \, \log \theta_{m_i, k_i} \\
	& = & \displaystyle \sum_m \log \pi_{m} \sum_{i=1}^n \ind{m_i = m} \; + \; \sum_k \sum_m \log \theta_{m, k} \sum_{i=1}^n \ind{m_i = m, k_i = k} \\
	\end{array} $$
	On peut alors maximiser $\pi$ et $\theta$ indépendamment.
	
	\paragraph{}
	Commençons par $\pi$. Nous avons la contrainte :
	\begin{equation}
	\label{pi_cons}
	\sum_m \pi_m = 1
	\end{equation}
	Ce qui donne le Lagrangien :
	$$ h_1(\pi, \nu) = - \sum_m \log \pi_{m} \sum_{i=1}^n \ind{m_i = m} \; + \; \nu \left(\sum_m \pi_m - 1 \right) $$
	On calcule alors la dérivé partielle de ce Lagrangien par rapport à $\pi_m$ :
	$$ \dfrac{\partial h_1}{\partial \pi_m} = - \dfrac{1}{\pi_m} \sum_{i=1}^n \ind{m_i = m} \; + \; \nu $$
	Pour $\pi$ optimal ces dérivées partielles sont nulles. D'où :
	$$ \pi_m = \dfrac{1}{\nu} \sum_{i=1}^n \ind{m_i = m} $$
	Avec la contrainte \eqref{pi_cons} on peut déterminer $\nu$ :
	$$ \nu = \sum_m \sum_{i=1}^n \ind{m_i = m} = \sum_{i=1}^n \sum_m \ind{m_i = m} = \sum_{i=1}^n 1 = n $$
	Ce qui donne :
	$$ \pi_m = \dfrac{1}{n} \sum_{i=1}^n \ind{m_i = m} $$
	
	\paragraph{}
	Maintenant maximisons $L_{m, k}$ selon $\theta$. Cette fois-ci nous avons les $M$ contraintes suivantes :
	\begin{equation}
	\label{th_cons}
	\forall m, \; \sum_k \theta_{m, k} = 1
	\end{equation}
	Le Lagrangien s'écrit alors :
	$$ h_2(\theta, \nu) = - \sum_k \sum_m \log \theta_{m, k} \sum_{i=1}^n \ind{m_i = m, k_i = k} + \sum_m \nu_m \left( \sum_k \theta_{m, k} - 1 \right) $$
	On calcule ensuite la dérivé partielle de ce Laplacien par rapport à $\theta_{m, k}$ :
	$$ \dfrac{\partial h_2}{\partial \theta_{m, k}} = - \dfrac{1}{\theta_{m, k}} \sum_{i=1}^n \ind{m_i = m, k_i = k} \; + \; \nu_m $$
	Cette dérivée devant être nulle pour pouvoir maximiser la log-vraisemblance, on obtient :
	$$ \theta_{m, k} = \dfrac{1}{\nu_m} \sum_{i=1}^n \ind{m_i = m, k_i = k} $$
	Ensuite on utilise la contrainte \eqref{th_cons} pour obtenir $\nu_m$ :
	$$ \nu_m = \sum_k \sum_{i=1}^n \ind{m_i = m, k_i = k} = \sum_{i=1}^n \ind{m_i = m} \sum_k \ind{k_i = k} = \sum_{i=1}^n \ind{m_i = m} = n \pi_m $$
	Si $\pi_m$ est nul alors cela veut dire qu'aucun échantillon avec $z = m$ n'a été observé. On a alors aucune information sur la loi de $x$ sachant $z = m$ et donc on peut prendre n'importe quelle vecteur à valeurs positives dont la somme des coefficients fait 1 pour $\theta_m$. Sinon on a :
	$$ \theta_{m, k} = \dfrac{1}{n \pi_m} \sum_{i=1}^n \ind{m_i = m, k_i = k} $$
	
	\paragraph{}
	Pour résumer voici deux estimateurs par maximum de vraisemblance possibles :
	\begin{center}
		\fbox{$\displaystyle \hat{\pi}_m^{MLE} = \dfrac{1}{n} \sum_{i=1}^n \ind{z_i = m} \qquad \hat{\theta}_{m, k}^{MLE} = \syst{ll}{
				\displaystyle \dfrac{1}{n \hat{\pi}_m^{MLE}} \sum_{i=1}^n \ind{z_i = m, x_i = k} & \text{Si } \hat{\pi}_m^{MLE} > 0 \\
				\dfrac{1}{K} & \text{Si } \hat{\pi}_m^{MLE} = 0
			}$}
	\end{center}
	
	\section{Classification linéaire}
	
	\subsection{Modèle génératif (LDA)}
	
	\paragraph{a.}
	Comme dans l'exercice précédent on commence par introduire la log-vraisemblance :
	$$ \begin{array}{lll}
	L_N (\pi, \mu_0, \mu_1, \Sigma^{-1}) & = & \displaystyle \sum_{n=1}^N \log p \left( x_n, y_n \mid \pi, \mu_0, \mu_1, \Sigma^{-1} \right) \\
	& = & \displaystyle \sum_{n=1}^N \log \left[ p(y_n | \pi) p \left( x_n | y_n, \mu_0, \mu_1, \Sigma^{-1} \right) \right] \\
	\\
	& = & \displaystyle \sum_{n=1}^N \left( y_n \log \pi \, + \, (1 - y_n) \log (1 - \pi) \right) \\
	& & \displaystyle \quad + \; \dfrac{N}{2} \log \left| \Sigma^{-1} \right| \; - \; N \log(2 \pi) \quad - \quad \sum_{n=1}^N \dfrac{1}{2} \left( x_n - \mu_{y_n} \right)^\trans \Sigma^{-1} \left ( x_n - \mu_{y_n} \right) \\
	\end{array} $$
	On dérive alors la log-vraisemblance par rapport à $\pi$ et on obtient :
	$$ \dfrac{\partial L_N}{\partial \pi} = \sum_{n=1}^N \dfrac{y_n}{\pi} - \dfrac{1 - y_n}{1 - \pi} $$
	Comme $L_N$ est concave en $\pi$, $L_N$ admet un maximum lorsque la dérivée précédente est nulle :
	$$ \dfrac{\partial L_N}{\partial \pi} = 0 \Leftrightarrow \sum_{n=1}^N (1 - \pi) y_n - \pi (1 - y_n) = 0 \Leftrightarrow  \pi = \dfrac{1}{N} \sum_{n=1}^N y_n $$
	De la même manière, $L_N$ est concave en $\mu_0$ et $\mu_1$. Il suffit alors d'annuler les gradients de $L_N$ par rapport à ces deux paramètres pour obtenir les estimateurs par maximum de vraisemblance. On commence par $\mu_0$. Les seules termes de la somme, qui figurent dans le gradient sont ceux dont $y_n = 0$:
	$$ \nabla_{\mu_0} L_N = \sum_{n=1}^N  \Sigma^{-1} (1-y_n) \left( x_n - \mu_0 \right) = 0 \Leftrightarrow  \Sigma^{-1} \left(\sum_{n=1}^N(1- y_n) x_n - \sum_{n=1}^N (1-y_n) \mu_0 \right) = 0 $$
	Or comme $\Sigma^{-1}$ est inversible, 
	$$ \left(\sum_{n=1}^N (1 - y_n) x_n \right) - N_0 \mu_0  = 0 \Leftrightarrow   \mu_0  = \dfrac{1}{N_0} \sum_{n=1}^N (1-y_n) x_*n $$
	Où $N_0 = \sum_{n=1}^N (1 - y_n)$ est le nombre d'observations qui appartiennent à la classe $y_n = 0$
	De la même manière on obtient :
	$$ \mu_1  = \dfrac{1}{N_1} \sum_{n=1}^N y_n x_n  $$
	Avec $N_1 = \sum_{n=1}^N y_n$, le nombre d'observations qui appartiennent à la classe $y_n = 1$. \\ 
	Reste à estimer $\Sigma^{-1}$. Pour ce faire on commence par calculer la différentielle de la fonction $f : S \mapsto \log | S |$ définie sur l'ensemble des matrices symétriques définies positives de dimension 2.
	$$ \begin{array}{lll}
	| S + H | & = & (s_{11} + h_{11}) (s_{22} + h_{22}) - (s_{12} + h_{12}) (s_{21} + h_{21}) \\
	& = & | S | + s_{11} h_{22} + s_{22} h_{11} - s_{12} h_{21} - s_{21} h_{12} + o(\|H\|) \\
	& = & |S| + \text{trace}(\com(S)^\trans H) + o(\|H\|)  \\
	& = & |S| + |S| \text{trace}(S^{-1} H) + o(\|H\|) 
	\end{array} $$
	Par symétrie de $S^{-1}$ on obtient le gradient de $f$ suivant :
	$$ \nabla f(S) = \dfrac{1}{|S|} (\nabla | \, . \, |)(S) = S^{-1} $$
	On calcule désormais le gradient de $L_N$ par rapport à $\Sigma^{-1}$ :
	$$ \nabla_{\Sigma^{-1}} L_N = \dfrac{N}{2} \Sigma - \dfrac{1}{2} \sum_{n=1}^N (x_n - \mu_{y_n}) (x_n - \mu_{y_n})^\trans $$
	Comme ce gradient doit être nul pour maximiser $L_n$, on obtient finalement l'estimation de $\Sigma$ suivante :
	$$ \Sigma = \dfrac{1}{N} \sum_{n=1}^n (x_n - \mu_{y_n}) (x_n - \mu_{y_n})^\trans $$
	
	Finalement on conclu par les expressions suivantes pour nos estimateurs par maximum de vraisemblance :
	\begin{center}
		\fbox{$\displaystyle \pi = \dfrac{N_1}{N} \qquad \mu_0 = \dfrac{1}{N_0} \sum_{n=1}^N (1-y_n) x_n \qquad \mu_1 = \dfrac{1}{N_1} \sum_{n=1}^N y_n x_n \qquad \Sigma = \dfrac{1}{N} \sum_{n=1}^N (x_n - \mu_{y_n}) (x_n - \mu_{y_n})^\trans$}
	\end{center}
	
	
	\paragraph{b.}
	On utilise la formule de Bayes pour obtenir la forme de la probabilité conditionnelle donnée~:
	$$ \begin{array}{lll}
	p(y = 1 \mid x) & = & \displaystyle \dfrac{p(x \mid y = 1) p(y = 1)}{p(x)} = \dfrac{p(x \mid y = 1) p(y = 1)}{p(x | y = 0)p(y = 0) + p(x \mid y = 1)p(y = 1)} \\[5mm]
	& = & \displaystyle \dfrac{\pi \exp \left( - \dfrac{1}{2} (x - \mu_1)^\trans \Sigma^{-1} (x - \mu_1) \right)}{(1 - \pi) \exp \left( - \dfrac{1}{2} (x - \mu_0)^\trans \Sigma^{-1} (x - \mu_0) \right) \, + \, \pi \exp \left( - \dfrac{1}{2} (x - \mu_1)^\trans \Sigma^{-1} (x - \mu_1) \right)} \\[8mm]
	& = & \displaystyle \dfrac{1}{1 + \exp \left( \ln \dfrac{1 - \pi}{\pi} \, - \, \dfrac{1}{2} (x - \mu_0)^\trans \Sigma^{-1} (x - \mu_0) \, + \, \dfrac{1}{2} (x - \mu_1)^\trans \Sigma^{-1} (x - \mu_1) \right)} \\[8mm]
	& = & \dfrac{1}{1 + \exp(-g(x))} = sigm(g(x))
	\end{array} $$
	On remarque ensuite que notre fonction $g$ est en réalité une fonction affine en $x$. On écrit alors :
	\begin{center}
		\fbox{$\displaystyle p(y = 1 \mid x) = sigm(w^\trans x + b) \qquad $ Avec $ \qquad \displaystyle \syst{lll}{
				w & = & \Sigma^{-1} (\mu_1 - \mu_0) \\[2mm]
				b & = & \dfrac{1}{2} \mu_0^\trans \Sigma^{-1} \mu_0 \, - \, \dfrac{1}{2} \mu_1^\trans \Sigma^{-1} \mu_1 \, - \, \ln \dfrac{1 - \pi}{\pi}
			}$}
	\end{center}
	On reconnaît donc la même forme qu'avec une régression logistique.
	
	\paragraph{c.}
	Pour pouvoir afficher la séparation des classes proposées, on remarque l'équivalence suivante :
	$$ p(y = 1 \mid x) = 0.5 \Leftrightarrow w^\trans x + b = 0 $$
	Voici alors le résultat obtenu :
	\begin{figure}[h]
		\includegraphics[width=\linewidth]{lda.png}
		\caption{Séparations des clusters obtenus pour les 3 jeux de données avec le modèle LDA}
	\end{figure}

	\subsection{Régression logistique}
	
	Pour la régression logistique on considère les paramètre $w$ et $b$ sous un seul vecteur noté $\theta$ :
	$$ \theta = \pmat{w^\trans & b}^\trans $$
	Afin d'avoir un modèle purement linéaire on ajoute aussi une nouvelle coordonnées qui vaut toujours 1 à nos points :
	$$ X_n = \pmat{x_n^\trans & 1}^\trans $$
	Ainsi on a :
	$$ w^\trans x_n + b = \theta^\trans X_n $$
	On définit ensuite la probabilité conditionnelle de $y$ sachant $x$ à l'aide de la fonction $h_\theta$ :
	$$ p(y = 1 \mid x, \theta) = h_\theta(X) = \dfrac{1}{1 + e^{- \theta^\trans X}} $$
	Plus généralement :
	$$ p(y | x, \theta) = h_\theta(x)^y \left( 1 - h_\theta(x) \right)^{1-y} $$
	On en déduit ensuite la log-vraisemblance :
	$$ L(\theta) = \sum_{n = 1}^N y_n \ln \left( h_\theta(X_n) \right) + (1 - y_n) \ln \left( 1 - h_\theta(X_n) \right) $$
	On veut maximiser cette vraisemblance. Pour cela on utilisera la méthode de Newton. Il nous faut donc calculer le gradient. Dans ce but, on remarque d'abord l'égalité :
	$$ h_\theta (X_n) = h_{X_n}(\theta) $$
	Puis en calculant le gradient de $h_{X_n}$ on obtient :
	$$ \nabla h_{X_n} (\theta) = X_n h_{X_n}(\theta) \left( 1 - h_{X_n}(\theta) \right) $$
	Finalement on est dans la mesure de calculer le gradient de $L$ :
	$$ \nabla L(\theta) = \sum_{n = 1}^N y_n X_n \left( 1 - h_{X_n}(\theta) \right) - (1 - y_n) X_n h_{X_n}(\theta) = \sum_{n = 1}^N X_n \left( y_n - h_\theta (X_n) \right) $$
	En posant $X = \pmat{X_1 & \cdots & X_N}^\trans$ et $y = \pmat{y_1 & \cdots & y_N}^\trans$, on obtient :
	\begin{center}
		\fbox{$ \displaystyle \nabla L(\theta) = X^\trans \left( y - h_\theta(X) \right) $}
	\end{center}
	On calcule désormais la Hessienne :
	$$ H = - \sum_{n = 1}^N X_n \nabla h_{X_n} (\theta)^\trans = - \sum_{n = 1}^N X_n X_n^\trans h_{X_n}(\theta) \left( 1 - h_{X_n}(\theta) \right) $$
	Ce qui donne :
	\begin{center}
		\fbox{$ \displaystyle H = - \sum_{n = 1}^N h_\theta(X_n) \left( 1 - h_\theta(X_n) \right) X_n X_n^\trans $}
	\end{center}

	\paragraph{a.}
	On note $w_A$ et $b_A$ les paramètres appris avec les données dans \verb|trainA|. On utilise des notations similaires pour les deux autres jeux de données. Voici les valeurs numériques obtenues~:
	$$ w_A = \pmat{0.35236 & -0.93587}^\trans \qquad b_A = 4.12304 $$
	$$ w_B = \pmat{0.43587 & -0.90001}^\trans \qquad b_B = 3.24739 $$
	$$ w_C = \pmat{0.07337 & -0.99730}^\trans \qquad b_C = 9.02859 $$
	
	\paragraph{b.}
	On obtient la séparation \figurename~\ref{log}.
	\begin{figure}[h]
		\includegraphics[width=\linewidth]{logistic.png}
		\caption{Séparations des clusters obtenus pour les 3 jeux de données avec la régression logistique}
		\label{log}
	\end{figure}

	\subsection{Régression linéaire}
	
	On reprend notre vecteur $\theta$. En régression linéaire, on souhaite minimiser la fonction de coût convexe suivante :
	$$ J(\theta) = \dfrac{1}{2} \left\| X \theta - y \right\|_2^2 $$
	Cette fonction admet un minimum là où son gradient s'annule. Ce qui donne :
	$$ \nabla J(\theta) = X^\trans X \theta - X^\trans y = 0 $$
	Ainsi si la matrice $X^\trans X$ est inversible on obtient l'expression suivante de $\theta$ :
	\begin{center}
		\fbox{$ \displaystyle \theta = \left( X^\trans X \right)^{-1} X^\trans y $}
	\end{center}

	\paragraph{a.}
	On note $w_A$ et $b_A$ les paramètres appris avec les données dans \verb|trainA|. On utilise des notations similaires pour les deux autres jeux de données. Voici les valeurs numériques obtenues~:
	$$ w_A = \pmat{0.05582 & -0.17637}^\trans \qquad b_A = 1.38346 $$
	$$ w_B = \pmat{0.08258 & -0.14758}^\trans \qquad b_B = 0.88250 $$
	$$ w_C = \pmat{0.01675 & -0.15897}^\trans \qquad b_C = 1.64015 $$
	
	\paragraph{b.}
	Comme la ligne de séparation des deux classes est définie par $f(x) = 0.5$ au lieu de $f(x) = 0$ on soustrait ensuite la valeur 0.5 à $b$. On note cette valeur $b'$. De telle sorte que :
	$$ p(y = 1 \mid x) = 0.5 \Leftrightarrow w^\trans x + b' = 0 $$
	La \figurename~\ref{lin} montre les séparations linéaires obtenues entre les deux clusters.
	\begin{figure}[h]
		\includegraphics[width=\linewidth]{linear.png}
		\caption{Séparations des clusters obtenus pour les 3 jeux de données avec la régression linéaire}
		\label{lin}
	\end{figure}

	\subsection{Applications}
	
	\paragraph{a.}
	\label{Tab}
	Voici un tableau récapitulatif des performances (en incluant le modèle QDA de la section suivante) : \\[3mm]
	\makebox[\textwidth][c]{
		\begin{tabular}{||l||c|c|c|c|c|c||}
			\hline
			\multirow{2}{*}{Modèle} & \multicolumn{3}{c|}{précision/erreur apprentissage} & \multicolumn{3}{c||}{précision/erreur test} \\
			\cline{2-7}
			& A & B & C & A & B & C \\
			\hline \hline
			LDA & 1 / 0 & 0.98 / 0.02 & 0.973 / 0.027 & 0.99 / 0.01 & 0.955 / 0.045 & 0.96 / 0.04 \\
			\hline
			Logistique & 1 / 0 & 0.98 / 0.02 & 0.977 / 0.023 & 0.99 / 0.01 & 0.96 / 0.04 & 0.95 / 0.05 \\
			\hline
			Linéaire & 1 / 0 & 0.98 / 0.02 & 0.973 / 0.027 & 0.99 / 0.01 & 0.955 / 0.045 & 0.96 / 0.04 \\
			\hline
			QDA & 1 / 0 & 0.99 / 0.01 & 0.97 / 0.03 & 0.99 / 0.01 & 0.97 / 0.03 & 0.96 / 0.04 \\
			\hline
		\end{tabular}
	}

	\paragraph{b.}
	En pratique, les performances de ces méthodes semblaient assez bonnes.
	Ils n'ont pas besoin d’un nombre très grand de points d’entraînement.
	L'erreur sur les données d'apprentissage est toujours plus petite que l'erreur sur les données de test, ce qui est prévisible puisque les paramètres des modèles sont déduits à partir des données d'apprentissages.
	
	LDA, et la régression linéaire donnent des séparations quasi identiques (les paramètres de la droite de séparation sont égaux à 3 chiffres significatif près). Ainsi ces deux modèles ont exactement les même erreurs. On peut se demander si ces deux modèles sont équivalents et que la différence observé provient d'erreurs numériques. La réponses est non mais les expressions des directions des lignes séparatrices ont des expressions mathématiques très proches.
	
	Exprimons plus en détail la valeur de $w$ dans le cas de la régression linéaire. On note $C$ la matrice de covariance des observations et $\mu$ leur moyenne :
	$$ C = \dfrac{1}{N} \sum_{n=1}^N x_n x_n^\trans \qquad \mu = \dfrac{1}{N} \sum_{n = 1}^N x_n $$
	Cela nous permet de réécrire la matrice $X^\trans X$ par :
	$$ X^\trans X = N \pmat{C & \mu \\ \mu^\trans & 1} \qquad \left( X^\trans X \right)^{-1} = \dfrac{1}{N} \pmat{A & v \\ v^\trans & a} $$
	On obtient alors le système :
	$$ \syst{lllr}{
		CA + \mu v^\trans & = & I_2 & (1) \\
		Cv + \mu a & = & 0 & (2) \\
		\mu^\trans A + v^\trans & = & 0 & (3) \\
		\mu^\trans v + a & = & 1 & (4)
	} $$
	En injectant la troisième équation dans la première on obtient :
	$$ CA - \mu \mu^\trans A = I_2 \Leftrightarrow A = \left( C - \mu \mu^\trans \right)^{-1} $$
	On exprime maintenant $w_{lin}$ à l'aide de cette nouvelle matrice :
	$$ \begin{array}{lll}
		w_{lin} & = & \dfrac{1}{N} \pmat{A & v} X^\trans y \\[3mm]
		& = & \displaystyle \dfrac{1}{N} \pmat{A & -A \mu} \pmat{ \sum_{n = 1}^N y_n x_n \\[1mm] \sum_{n = 1}^N y_n } \\[3mm]
		& = & \displaystyle \dfrac{N_1}{N} A \left( \mu_1 - \mu \right)
	\end{array} $$
	On exprime maintenant $\mu$ en fonction des moyennes sur les deux classes :
	$$ \mu = \dfrac{1}{N} \sum_{n = 1}^N x_n = \dfrac{1}{N} \left[ \sum_{n = 1}^N y_n x_n + \sum_{n = 1}^N (1 - y_n) x_n \right] = \dfrac{N_1 \mu_1 + N_0 \mu_0}{N} $$
	On obtient ensuite :
	$$ \mu_1 - \mu = \dfrac{N \mu_1 - N_1 \mu_1 - N_0 \mu_0}{N} = \dfrac{N_0}{N} \left( \mu_1 - \mu_0 \right) $$
	D'où :
	\begin{center}
		\fbox{$ \displaystyle w_{lin} = \pi (1 - \pi) \left( C - \mu \mu^\trans \right)^{-1} \left( \mu_1 - \mu_0 \right) $}
	\end{center}
	On rappelle la valeur de $w$ pour LDA :
	$$ w_{LDA} = \Sigma^{-1} \left( \mu_1 - \mu_0 \right) $$
	Comme la direction de la séparation de dépend pas de la norme de $w$, il suffit de regarder si $\Sigma$ et $C + \mu \mu^\trans$ sont proportionnels pour vérifier une possible équivalence entre les deux modèles.
	$$ \begin{array}{lll}
		\Sigma & = & \displaystyle \dfrac{1}{N} \sum_{n=1}^N (x_n - \mu_{y_n}) (x_n - \mu_{y_n})^\trans \\[3mm]
		& = & \displaystyle C - \dfrac{1}{N} \sum_{n = 1}^N x_n \mu_{y_n}^\trans + \mu_{y_n} x_n^\trans + \mu_{y_n} \mu_{y_n}^\trans \\[3mm]
		& = & \displaystyle C \; - \; \dfrac{2}{N} \sum_{n = 1}^N x_n \mu_{y_n}^\trans \; + \; \pi \mu_1 \mu_1^\trans \; + \; (1 - \pi) \mu_0 \mu_0^\trans
	\end{array} $$
	Ensuite on remarque l'égalité suivante :
	$$ \mu_{y_n} = y_n \mu_1 + (1 - y_n) \mu_0 $$
	D'où :
	$$ \sum_{n = 1}^N x_n \mu_{y_n}^\trans = \sum_{n = 1}^N y_n x_n \mu_1^\trans + (1 - y_n) x_n \mu_0^\trans = N_1 \mu_1 \mu_1^\trans + N_0 \mu_0 \mu_0^\trans $$
	Finalement voici, la nouvelle expression de $\Sigma$ :
	\begin{center}
		\fbox{$ \displaystyle \Sigma = C \; - \; \pi \mu_1 \mu_1^\trans \; - \; (1 - \pi) \mu_0 \mu_0^\trans $}
	\end{center}
	On en conclue qu'il n'y a pas d'équivalence entre le modèle LDA et la régression linéaire, mais les deux expressions de $w$ sont très proches :
	$$ w_{lin} \propto \left( C - \mu \mu^\trans \right)^{-1} \left( \mu_1 - \mu_0 \right) \qquad w_{LDA} = \left( C - \pi \mu_1 \mu_1^\trans - (1 - \pi) \mu_0 \mu_0^\trans \right)^{-1} \left( \mu_1 - \mu_0 \right) $$
	
	\subsection{Modèle QDA}
	
	On réécrit la nouvelle log-vraisemblance :
	$$ \begin{array}{lll}
	L_N (\pi, \mu_0, \mu_1, \Sigma_0^{-1}, \Sigma_1^{-1})
	& = & \displaystyle \sum_{n=1}^N \left( y_n \log \pi \, + \, (1 - y_n) \log (1 - \pi) \right) \\[3mm]
	& & \displaystyle \quad + \; \dfrac{N_0}{2} \log \left| \Sigma_0^{-1} \right| \; + \; \dfrac{N_1}{2} \log \left| \Sigma_1^{-1} \right| \; - \; N \log(2 \pi) \\[3mm]
	& & \quad - \; \sum_{n=1}^N \dfrac{1}{2} \left( x_n - \mu_{y_n} \right)^\trans \Sigma_{y_n}^{-1} \left ( x_n - \mu_{y_n} \right) \\
	\end{array} $$
	On remarque alors que l'estimateur pour $\pi$ ne change pas. En reprenant les calculs des espérances $\mu_0$ et $\mu_1$ on remarque aussi que leurs estimateurs sont aussi inchangés. Enfin il nous faut calculer les estimateurs par maximum de vraisemblance de nos deux nouvelles matrices de covariances :
	$$ \nabla_{\Sigma_0^{-1}} L_N = \dfrac{N_0}{2} \Sigma_0 - \dfrac{1}{2} \sum_{n = 1}^N (1 - y_n) \left( x_n - \mu_0 \right)^\trans \left( x_n - \mu_0 \right) $$
	$$ \nabla_{\Sigma_1^{-1}} L_N = \dfrac{N_1}{2} \Sigma_1 - \dfrac{1}{2} \sum_{n = 1}^N y_n \left( x_n - \mu_1 \right)^\trans \left( x_n - \mu_1 \right) $$
	Ce qui donne les estimateurs :
	\begin{center}
		\fbox{$ \displaystyle \Sigma_0 = \dfrac{1}{N_0} \sum_{n = 1}^N (1 - y_n) \left( x_n - \mu_0 \right)^\trans \left( x_n - \mu_0 \right) \qquad \Sigma_1 = \dfrac{1}{N_1} \sum_{n = 1}^N y_n \left( x_n - \mu_1 \right)^\trans \left( x_n - \mu_1 \right) $}
	\end{center}

	\paragraph{a.}
	Voici les valeurs numériques obtenues~: \\[2mm]
	\makebox[\textwidth][c]{$ \pi^A = 0.48 \quad \mu_0^A = \pmat{10.7325 \\ 10.9398} \quad \mu_1^A = \pmat{11.0326 \\  5.9929} \quad \Sigma_0^A = \pmat{0.4646 & 0.0989 \\ 0.0989 & 0.7132} \quad \Sigma_1^A = \pmat{0.7221 & 0.1827 \\ 0.1827 & 0.9348} $
	} \\[2mm]
	\makebox[\textwidth][c]{$ \pi^B = 0.55 \quad \mu_0^B = \pmat{10.5826 \\ 11.1717} \quad \mu_1^B = \pmat{11.2476 \\  6.0953} \quad \Sigma_0^B = \pmat{0.7616 & 0.0535 \\ 0.0535 & 1.1074} \quad \Sigma_1^B = \pmat{2.3658 & 1.2314 \\ 1.2314 & 2.8404} $
	} \\[2mm]
	\makebox[\textwidth][c]{
	$ \pi^C = 0.4167 \quad \mu_0^C = \pmat{10.6192 \\ 10.8387} \quad \mu_1^C = \pmat{11.1846 \\  6.0425} \quad \Sigma_0^C = \pmat{1.2858 & -0.4336 \\ -0.4336 & 1.8260} \quad \Sigma_1^C = \pmat{1.2677 & 0.4571 \\ 0.4571 & 1.4416} $
	}

	\paragraph{b.}
	On recalcule la probabilité $p(y = 1 \mid x)$ avec nos nouvelles expressions :
	$$ \begin{array}{lll}
	p(y = 1 \mid x)	& = & \displaystyle \dfrac{1}{1 + \exp \left( \ln \dfrac{1 - \pi}{\pi} \, - \, \dfrac{1}{2} (x - \mu_0)^\trans \Sigma_0^{-1} (x - \mu_0) \, + \, \dfrac{1}{2} (x - \mu_1)^\trans \Sigma_1^{-1} (x - \mu_1) \right)} \\[8mm]
	& = & \dfrac{1}{1 + \exp(-g(x))} = sigm(g(x))
	\end{array} $$
	Puis $p(y = 1 \mid x) = 0.5$ est équivalent à $g(x) = 0$. Ce qui donne :
	$$ \begin{array}{lll}
	p(y = 1 \mid x) = 0.5 & \Leftrightarrow & \ln \dfrac{1 - \pi}{\pi} \, - \, \dfrac{1}{2} (x - \mu_0)^\trans \Sigma_0^{-1} (x - \mu_0) \, + \, \dfrac{1}{2} (x - \mu_1)^\trans \Sigma_1^{-1} (x - \mu_1) = 0 \\[4mm]
	& \Leftrightarrow & \dfrac{1}{2} x^\trans \left( \Sigma_0^{-1} - \Sigma_1^{-1} \right) x \; + \; x^\trans \left( \Sigma_1^{-1} \mu_1 - \Sigma_0^{-1} \mu_0 \right) \\[4mm]
	& & \quad + \; \dfrac{1}{2} \mu_0^\trans \Sigma_0^{-1} \mu_0 \, - \, \dfrac{1}{2} \mu_1^\trans \Sigma_1^{-1} \mu_1 \, - \, \ln \dfrac{1 - \pi}{\pi} = 0 \\[4mm]
	& \Leftrightarrow & x^\trans Q x + w^\trans x + b = 0
	\end{array} $$
	Avec :
	$$ \syst{lll}{
		Q & = & \displaystyle \dfrac{1}{2} \left( \Sigma_0^{-1} - \Sigma_1^{-1} \right) \\[4mm]
		w & = & \displaystyle \Sigma_1^{-1} \mu_1 - \Sigma_0^{-1} \mu_0 \\[3mm]
		b & = & \displaystyle \dfrac{1}{2} \mu_0^\trans \Sigma_0^{-1} \mu_0 \, - \, \dfrac{1}{2} \mu_1^\trans \Sigma_1^{-1} \mu_1 \, - \, \ln \dfrac{1 - \pi}{\pi}
	} \vspace{4mm} $$
	On obtient alors la séparation \figurename~\ref{qda} :
	\begin{figure}[h]
		\includegraphics[width=\linewidth]{qda.png}
		\caption{Séparations des clusters obtenus pour les 3 jeux de données avec le modèle QDA}
		\label{qda}
	\end{figure}

	\paragraph{c.}
	Voir le tableau récapitulatif des performances, section \ref{Tab}.
	
	\paragraph{d.}
	Le meilleur classifieur ici est QDA. Il  semble être très efficace sur les 3 jeux de données. Il généralise LDA, en relaxant l'hypothèse de la forme commune des matrices de covariance pour les 2 classes. Ce modèle est alors plus expressif et obtient une meilleur vraisemblance que LDA sur les données d'entraînement. Attention, cela ne signifie pas une erreur plus faible comme on peut le constater pour le dataset C.
	Ce nouveau modèle fonctionne surtout  mieux que LDA sur le dataset B. Cela s'interprète par le fait que les 2 classes ont des matrices de covariance qui diffèrent plus que dans les deux autres jeux de données (A et C). Ainsi il y a un réel plus à considérer des matrices de covariances différentes.
	
	Finalement avec l'augmentation de la complexité du modèle on pourrait éventuellement craindre du suraprentissage qui se manifesterait par des résultats moins bons sur les jeux de tests, mais heureusement ce n'est pas le cas. Le modèle reste toujours assez simple pour éviter ce genre de soucis. 
	
\end{document}