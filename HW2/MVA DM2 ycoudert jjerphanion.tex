\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\graphicspath{{images/}}

\input{code_header.tex}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Modèles graphiques probabiliste --- DM 2
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont, Julien Jerphanion}

\DeclareMathOperator{\com}{com}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\diag}{\text{\textbf{diag}}~}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\ind}[1]{\mathbbm{1}_{#1}}

\begin{document}
	
	\maketitle
	
	\section*{Classification: K-means, and the EM algorithm}
	
	\paragraph{1.}
	L'étape d'espérance consiste à calculer la probabilité de $Z$ conditionnellement à $X$ et $\theta_{t-1}$. Où $\theta_t = (p_k, \mu_k, D_k)$ est l'ensemble des paramètres à l'étape $t$. On a :
	$$ \tau_{ik}^t = P(Z_i = k \mid x_i, \theta_{t-1}) = \dfrac{P(Z_i = k \mid \theta_{t-1}) P(x_i \mid Z_i = k, \theta_{t-1})}{P(x_i \mid \theta_{t-1})} $$
	Comme le dénominateur ne dépend pas de $k$. Il suffit de calculer le numérateur puis de normaliser $\tau_i^t$ qui est la distribution de probabilité de $Z_i$ à l'étape $t$. On écrit alors la relation de proportionnalité :
	$$ \tau_{ik}^t \propto \dfrac{p_k^{t-1}}{\sqrt{\left| D_k^{t-1} \right|}} \exp \left( - \dfrac{1}{2} \left( x_i - \mu_k^{t-1} \right)^\trans \left( D_k^{t-1} \right)^{-1} \left( x_i - \mu_k^{t-1} \right) \right) $$
	On note $d$ la dimension de notre espace. Et on note $d_{kj}$ le $j$-ème terme de la diagonale de $D_k$. On réécrit la relation précédente en fonction de ces termes :
	\begin{center}
		\fbox{$ \displaystyle \tau_{ik}^t \propto \dfrac{p_k^{t-1}}{\sqrt{\prod_{j = 1}^d d_{kj}^{t-1}}} \exp \left( - \dfrac{1}{2} \sum_{j = 1}^d \dfrac{1}{d_{kj}^{t-1}}\left( x_{ij} - \mu_{kj}^{t-1} \right)^2 \right)$}
	\end{center}
	
	\paragraph{}
	L'étape de maximisation consiste à trouver :
	$$ \theta_t = \argmax_\theta \sum_{i = 1}^n \sum_{k = 1}^K \tau_{ik}^t \log P(x_i, Z_i = k \mid \theta) $$
	On ré-exprime la valeur à maximiser en fonction de nos différents paramètres :
	$$ \theta_t = \argmax_\theta \sum_{i = 1}^n \sum_{k = 1}^K \tau_{ik}^t \left( \log p_k - \dfrac{1}{2} \sum_{j = 1}^d \left[ \dfrac{1}{d_{kj}} \left( x_{ij} - \mu_{kj} \right)^2 - \log d_{kj} \right] \right) $$
	
	\paragraph{}
	On peut maximiser les $p_k$ indépendamment des autres paramètres. On dispose de la contrainte que le vecteur $p$ est unitaire. On a alors le Lagrangien suivant :
	$$ L(p, \nu) = - \sum_{i = 1}^n \sum_{k = 1}^K \tau_{ik}^t \log p_k + \nu \left( \sum_{k = 1}^K p_k - 1 \right) $$
	Par convexité et stricte admissibilité, la dualité forte tient. Ainsi, si $(p, \nu)$ est optimal, le gradient de $L$ selon $p$ doit être nul. D'où :
	$$ \dfrac{\partial L}{\partial p_k} = \nu - \sum_{i = 1}^n \dfrac{\tau_{ik}^t}{p_k} = 0 \quad \Rightarrow \quad p_k = \dfrac{1}{\nu} \sum_{i = 1}^n \tau_{ik}^t $$
	Ensuite on utilise le fait que la somme des $p_k$ vaut 1 pour trouver $\nu$ :
	$$ \sum_{k = 1}^K p_k = \dfrac{1}{\nu} \sum_{k = 1}^K \sum_{i = 1}^n \tau_{ik}^t = \dfrac{n}{\nu} = 1 \quad \Rightarrow \quad \nu = n $$
	Finalement on obtient :
	\begin{center}
		\fbox{$ \displaystyle p_k^t = \dfrac{1}{n} \sum_{i = 1}^n \tau_{ik}^t $}
	\end{center}

	\paragraph{}
	Ensuite maximiser $L$ selon $\mu_{kj}$ revient à minimiser l'expression suivante :
	$$ \mu_{kj}^t = \argmin_{\mu_{kj}} \sum_{i = 1}^n \dfrac{\tau_{ik}^t}{d_{kj}} \left( x_{ij} - \mu_{kj} \right)^2 $$
	Ici $d_{kj}$ est juste un facteur multiplicatif positif. On peut donc l'enlever notre fonction à minimiser. En un point optimal, le gradient de notre fonction à minimiser est nul. On obtient alors~:
	$$ \sum_{i = 1}^n \tau_{ik}^t \left( \mu_{kj}^t - x_{ij} \right) = 0 \quad \Rightarrow \quad \mu_{kj}^t = \dfrac{1}{n p_k^t} \sum_{i = 1}^n \tau_{ik}^t x_{ij} $$
	On donne alors la relation sur le vecteur $\mu_k$ entier :
	\begin{center}
		\fbox{$ \displaystyle \mu_k^t = \dfrac{1}{n p_k^t} \sum_{i = 1}^n \tau_{ik}^t x_i $}
	\end{center}

	\paragraph{}
	Enfin on peut maximiser selon $d_{kj}$ connaissant les autres paramètres optimaux. Pour rappelle on a :
	$$ d_{kj}^t = \argmin_{d_{kj}} \sum_{i = 1}^n \tau_{ik}^t \left( \log d_{kj} + \dfrac{1}{d_{kj}} \left( x_{ij} - \mu_{kj}^t \right)^2 \right) $$
	On annule alors le gradient de cette fonction selon $d_{kj}$ :
	$$ \sum_{i = 1}^n \tau_{ik}^t \left( \dfrac{1}{d_{kj}^t} - \left( \dfrac{1}{d_{kj}^t} \right)^2 \left( x_{ij} - \mu_{kj}^t \right)^2 \right) = 0 $$
	Finalement on obtient l'expression des termes des diagonales des $D_k$ suivante :
	\begin{center}
		\fbox{$ \displaystyle d_{kj}^t = \dfrac{1}{n p_k^t} \sum_{i = 1}^n \tau_{ik} \left( x_{ij} - \mu_{kj}^t \right)^2 $}
	\end{center}

	\paragraph{2.}
	L'avantage de ce modèle peut être de forcer l'indépendance des dimensions dans la distribution des $X_i$ connaissant la classe $Z_i = k$. Quand on sait qu'il y a indépendance des dimension, alors on a intérêt à utiliser ce modèle pour forcer la contrainte. De plus au lieu d'avoir un nombre de paramètres quadratique en la dimension, on obtient finalement un nombre linéaire en la dimension de paramètres à optimiser. La convergence est donc probablement plus rapide avec ce modèle.
	
	\paragraph{3.}
	On affiche des ellipses pour chaque cluster correspondant à la gaussienne associée. Le centre de l'ellipse du cluster $k$ est $\mu_k$. Les directions principales de l'ellipse sont les vecteurs propres de la matrice de covariance $\Sigma_k$ ($D_k$ dans le modèle diagonal). Puis les longueurs des axes principaux sont les valeurs propres.
	
	On donne \figurename~\ref{first_comp} les résultats obtenus sur le jeu de donnés de IRIS en projetant sur les axes 0 et 3. Les projections sur les autres paires d'axes sont donnés en annexes \ref{other_comp}. Dans le cas des mixtures, des croix représentent le centre des gaussiennes et trois ellipses en point-tillés représentent la covariance. De plus l'épaisseur des ellipses donnent la probabilité $p$ d'appartenir à cette gaussienne.
	
	\begin{figure}
		\includegraphics[width=\linewidth]{clus30.png}
		\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 0 et 3.}
		\label{first_comp}
	\end{figure}
	
	\paragraph{4.}
	On s'intéresse aux cellules et aux frontières entre les clusters. Dans le cas de K-means, les cellules correspondent au diagramme de Voronoï des barycentres $\mu_k$. La frontière entre les clusters $k$ et $j$ est alors la médiatrice de $\mu_k$ et $\mu_j$. Les séparations sont alors des droites.
	
	Dans le cas des mélanges gaussiens, un point $x$ appartient au cluster $\argmax_k P(Z = k \mid x)$.
	Par proportionnalité des lois jointes et conditionnelles, le cluster est aussi $\argmax_k P(x, \, Z = k)$. La frontière entre les clusters $j$ et $k$ est alors défini par l'équation :
	$$ P(x, \, Z = k) = P(x, \, Z = j) $$
	On remplace par nos densités gaussiennes :
	$$ p_j \left| \Sigma_j \right|^{- \frac{1}{2}} \exp \left( - \dfrac{1}{2} \left( x - \mu_j \right)^\trans \Sigma_j^{-1} \left( x - \mu_j \right) \right) \quad = \quad p_k \left| \Sigma_k \right|^{- \frac{1}{2}} \exp \left( - \dfrac{1}{2} \left( x - \mu_k \right)^\trans \Sigma_k^{-1} \left( x - \mu_k \right) \right) $$
	En prenant le logarithme, on obtient alors une équation quadratique :
	$$ x^\trans \left( \Sigma_k^{-1} - \Sigma_j^{-1} \right) x + 2 \left( \Sigma_j^{-1} \mu_j - \Sigma_k^{-1} \mu_k \right)^\trans x + \left[ 2 \ln \dfrac{p_j}{p_k} + \ln \dfrac{|\Sigma_k|}{|\Sigma_j|} + \mu_k^\trans \Sigma_k^{-1} \mu_k - \mu_j^\trans \Sigma_j^{-1} \mu_j \right] \quad = \quad 0 $$
	Ainsi lorsque la séparation des clusters est quadratique mais pas linéaire les algorithmes EM seront plus adaptés. Ici nous prenons l'exemple d'un cluster de points distribués en cercles et d'un cluster de points dans une petite ellipse à l'intérieur du cercle du premier cluster. Les résultats obtenus sont donnés \figurename~\ref{outperf}.
	
	\begin{figure}[h]
		\includegraphics[width=\linewidth]{em_outperform.png}
		\caption{Exemple de cas dans lequel les algorithmes EM battent l'algorithme K-means. On observe la séparation des clusters par une droite (en noir) avec K-means et une séparation quadratique (ellipse épaisse) avec EM.}
		\label{outperf}
	\end{figure}

	\newpage
	\section*{Graphs, algorithms and Ising}
	
	\paragraph{1.}
	On autorise les nœuds de la chaîne à avoir des nombres d'états différents. On ne peut donc pas représenter l'ensemble des potentiels avec un seul \verb|array| de numpy. On note $k_i$ le nombre d'états du nœud $i$ et $n$ le nombre de nœuds dans la chaîne. Alors l'ensemble des potentiels de nœuds est représenté par une liste \verb|psi| de $n$ \verb|array| où $\psi[i]$ a pour \textit{shape} $(k_i,)$ et \verb|psi[i][a]| représente $\log \psi_i(a)$. Pour les potentiels d'arrêtes, on utilise une liste \verb|psi2| de $(n-1)$ \verb|array| où \verb|psi2[i]| a pour \textit{shape} $(k_i, k_{i+1})$ et \verb|psi2[i][a,b]| représente $\log \psi_{i, i+1}(a, b)$.
	
	\paragraph{2.}
	L'idée pour se ramener à une chaîne est de fusionner les lignes de notre grille pour former des nœuds ayant $2^w$ états. Le choix de fusionner les lignes vient du fait que les lignes sont plus petites que les colonnes dans le cas que l'on considère. En effet si on fusionnait les colonnes on aurait $2^{100}$ états ce qui est ingérable pour un ordinateur comparé au $2^{10}$ états de la fusion des lignes.
	
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}[ising/.style={draw, circle, minimum size=0.85cm}, scale=0.825]
			\node at (-1.5, -2.6) {$K=2$};
			\foreach \j in {0, ..., 4} {
				\foreach \i in {0, ..., 2} {
					\pgfmathtruncatemacro{\num}{\i + 3*\j}
					\node[ising] (\i\j) at (1.3*\i, -1.3*\j) {\num};
				}
				\draw[blue, thick] (1.3, -1.3*\j) ellipse (1.95cm and 0.6cm);
			}
			\foreach \j [count=\jj] in {0, ..., 3} {
				\foreach \i [count=\ii] in {0, ..., 1}
					\draw (\i\j) -- (\ii\j) (\i\j) -- (\i\jj);
				\draw (2\j) -- (2\jj);
			}
			\draw (04) -- (14) (14) -- (24);
			\draw[->, ultra thick, blue] (3.5, -2.6) -- node[above] {fusion} (5.5, -2.6);
			\node at (7.9, -2.6) {$K=2^w$};
			\foreach \j in {0, ..., 4}
				\node[ising] (\j) at (6.4, -1.3*\j) {\j};
			\foreach \j [count=\jj] in {0, ..., 3}
				\draw (\j) -- (\jj);
		\end{tikzpicture}
		\vspace{-2mm}
		\caption{Fusion des lignes}
		\label{junction}
	\end{figure}

	Dans la grille de départ on numérote le noeud d'abscisse $i$ et d'ordonné $j$ par l'indice $i+jw$ comme dans la \figurename~\ref{junction}. Le noeud $j$ de notre nouvelle chaîne est alors la fusion des noeuds $\left\{ i+jw \, ; \, i=0, ..., w-1 \right\}$. Les chiffres de l'écriture binaire des états de nos nouveaux noeuds correspondent aux états des noeuds de la ligne qui a été fusionné. On note $Y_j$ la variable aléatoire de notre nouveau noeud $j$, alors :
	$$ Y_j = a = a_0 2^0 + a_1 2^1 + ... + a_{w-1} 2^{w-1} \; \Leftrightarrow \; X_{0+jw} = a_0, \, X_{1+jw} = a_1, \, ..., \, X_{(w-1)+jw} = a_{w-1} $$
	Ainsi notre nouveau potentiel de noeud $\psi'$ s'exprime comme suit : 
	$$ \psi'_j(a) = \left( \prod_{i=0}^{w-1} \psi_{i+jw}(a_i) \right) \cdot \left( \prod_{i=0}^{w-2} \psi_{i+jw, i+1+jw}(a_i, a_{i+1}) \right)= \exp \left( \alpha \sum_{i=0}^{w-1} a_i + \beta \sum_{i=0}^{w-2} \mathbbm{1}_{a_i = a_{i+1}} \right) $$
	On note $|a|_1$ le nombre de 1 dans l'écriture binaire de $a$ et $\|a\|_{TV}$ la variation totale de l'écriture binaire de $a$, c'est à dire $\sum_{i=0}^{w-2} |a_{i+1} - a_i|$. On obtient alors :
	\begin{center}
		\fbox{$\displaystyle \psi'_j(a) = \exp \left( \alpha |a|_1 + \beta \left( w - 1 - \|a\|_{TV} \right) \right)$}
	\end{center}
	Pour calculer efficacement $|\cdot|_1$ on peut remarquer la relation suivante :
	$$ |0|_1 = 0 \qquad a > 0 \Rightarrow |a|_1 = 1 + \left| a - 2^{\lfloor \log_2 a \rfloor} \right|_1 $$
	Pour la variation totale on a la relation suivante lorsque $w > 1$ :
	$$ \| 0 \|_{TV} = 0 \quad \| 1 \|_{TV} = 1 \qquad 1 < a \Rightarrow \|a\|_{TV} = \left\| a - 2^{\lfloor \log_2 a \rfloor} \right\|_{TV} + 2 \mathbbm{1}_{a - 2^{\lfloor \log_2 a \rfloor} < 2^{\lfloor \log_2 a \rfloor - 1}} - \mathbbm{1}_{a \geqslant 2^{w-1}} $$
	Pour les nouveaux potentiels d'arrêtes on a :
	$$ \psi'_{j, j+1}(a, b) = \prod_{i=0}^{w-1} \psi_{i+jw, i+(j+1)w}(a_i, b_i) = \exp \left( \beta \sum_{i=0}^{w-1} \mathbbm{1}_{a_i = b_i} \right) $$
	En notant $\oplus$ l'opération XOR bit à bit on obtient :
	\begin{center}
		\fbox{$\displaystyle \psi'_{j, j+1}(a, b) = \exp \left( \beta \left( w - \left|a \oplus b \right|_1 \right) \right)$}
	\end{center}
	Connaissant les potentiels de notre chaîne on peut appliquer l'algorithme de la question précédente pour obtenir les messages et marginales. On est alors capable de calculer $Z$ via la formule suivante :
	\begin{center}
		\fbox{$\displaystyle Z = \sum_{a=0}^{2^w-1} \psi'_0(a) \mu_{1 \rightarrow 0}(a) $}
	\end{center}
	qui provient de l'expression des marginales en fonction des messages :
	$$ p(Y_0 = a) = \dfrac{1}{Z} \psi'_0(a) \mu_{1 \rightarrow 0}(a) $$
	Finalement on peut étudier le comportement lorsque $\beta$ tend vers plus ou moins l'infini avec $\alpha$ nul. On écrit l'expression de $\log Z$ suivante :
	$$ \log Z(0, \beta) = \log \left( \sum_x \exp \left( \beta \sum_{i \sim j} \mathbbm{1}_{x_i = x_j} \right) \right) $$
	Lorsque $\beta$ tend vers $-\infty$ alors tous les termes de la somme deviennent négligeables face aux deux termes correspondant aux $x$ en quinconce qui vérifient $\sum_{i \sim j} \mathbbm{1}_{x_i = x_j} = 0$. On a alors :
	$$ \log Z(0, \beta) \underset{\beta \rightarrow -\infty}{\sim} \log 2 $$
	Lorsque $\beta$ tend vers l'infini alors tous les termes de la somme deviennent négligeables face aux deux termes correspondant aux $x$ constants qui vérifient $\sum_{i \sim j} \mathbbm{1}_{x_i = x_j} = 2wh-w-h$. On a alors :
	$$ \log Z(0, \beta) \underset{\beta \rightarrow +\infty}{\sim} \log 2 + (2wh-w-h) \beta $$
	On peut donc utiliser ces deux asymptotes pour vérifier les résultats retournés par notre algorithme. Comme tous les termes de notre somme sont positifs, on sait aussi que la courbe de $\log Z$ se trouve au dessus de ces asymptotes. De plus nous avons implémenté une fonction brute force qui calcule les probabilités de tous les états de la grille pour trouver Z. Cela nous permet de vérifier que notre algorithme est correct pour des petites valeurs de $w$ et $h$ (par exemple $w=3$ et $h=5$).
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.55\linewidth]{Z.png}
		\caption{Courbe de $\log Z$ en fonction de $\beta$ pour $\alpha=0$, $w=10$ et $h=100$ en rouge. Les demis droites en traits-tillés sont les deux asymptotes théoriques.}
	\end{figure}
	
	\paragraph{3.}
	Nous avons implémenté un algorithme de \textit{loopy belief propagation} (voir \ref{LBP}), mais qui ne converge pas lorsque $w > 1$. Nous utilisons aucune normalisation. Pour $w=1$ on retrouve bien les mêmes valeurs de $Z$ qu'avec l'algorithme de la question précédente. Mais comme la convergence est exacte on ne peut pas répondre à la question "pour quelles valeurs de $\beta$ l'erreur d'approximation est plus grande ?".

	\newpage
	\appendix
	
	\section{Autres projections du jeu de donnés IRIS}
	\label{other_comp}
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.98\linewidth]{clus10.png}
		\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 0 et 1.}
	\end{figure}

	\begin{figure}
		\includegraphics[width=\linewidth]{clus20.png}
		\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 0 et 2.}
	\end{figure}

	\begin{figure}
	\includegraphics[width=\linewidth]{clus21.png}
	\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 1 et 2.}
	\end{figure}

	\begin{figure}
	\includegraphics[width=\linewidth]{clus31.png}
	\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 1 et 3.}
	\end{figure}

	\begin{figure}
	\includegraphics[width=\linewidth]{clus32.png}
	\caption{Comparaison des 3 algorithmes avec $K=2,3,4$. La projection est faite sur les axes 2 et 3.}
	\end{figure}

	\section{Code}
	
	\input{code.tex}

\end{document}