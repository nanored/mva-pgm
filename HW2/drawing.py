import pylab as pl
import numpy as np
from matplotlib.patches import Ellipse
from scipy.spatial import Voronoi, voronoi_plot_2d
pl.style.use("ggplot")

def plot_conic(coeffs, x0, x1, sx=200):
    """
    Return curves to draw the conic defined by the coefficients in `coeffs`

    Parameters
    ---------
    coeffs: list of float of size 6
        coeffs = [a, b, c, d, e, f] to draw the conic defined by:
        aX^2 + bXY + cY^2 + dX + eY + f = 0
    x0: float
        Abscissa from which we will draw the curve
    x1: float
        Abscissa to which we will draw the curve
    sx: int
        Number of points in the plot

    Return
    ------
    The result is a list of 0, 1, or 2 curves. Where a curve is represented by a list [xs, ys] with
    xs the list of abscissa of the curve and ys the list of ordinates
    """
    assert len(coeffs) == 6
    a, b, c, d, e, f = coeffs
    if c == 0:
        xs = np.linspace(x0, x1, sx)
        ys = - (a*xs**2 + d*xs + f) / (b*xs + e)
        return [[xs, ys]]
    xs, ys, ys2 = [], [], []
    void = True
    xb = x0-1
    for x in np.linspace(x0, x1, sx):
        A = c
        B = b*x + e
        C = x * (a*x + d) + f
        D = B*B - 4*A*C
        if D >= 0:
            sd = np.sqrt(D)
            y1 = (-B - sd) / (2*A)
            y2 = (-B + sd) / (2*A)
            xs.append(x)
            y1, y2 = sorted([y1, y2])
            ys.append(y1)
            ys2.append(y2)
            void = False
        elif void: xb = x
        else:
            void = True
            break
    if len(xs) == 0: return []
    if xb >= x0:
        xs = list(reversed(xs)) + xs
        ys = list(reversed(ys)) + ys2
        if void:
            xs.append(xs[0])
            ys.append(ys[0])
        return [[xs, ys]]
    elif void:
        xs += list(reversed(xs))
        ys += list(reversed(ys2))
        return [[xs, ys]]
    else:
        return [[xs, ys], [xs, ys2]]

def plot_sep(theta, x0, x1, d1=0, d2=1, sx=200):
	"""
	Just for the fun.
	Returns the projection on (d1, d2) of the curves corresponding to the separations
	between pairs of clusters i, j intersected with the image plane of the application q
	such that mu[i] and mu[j] belongs to the image of q and q composed with the
	projection on (d1, d2) is equal to identity.
	sx is the number of points in the curves
	"""
	p, mu, sigma = theta
	K, D = mu.shape
	mu = mu.reshape(K, D, 1)
	invSigma = np.linalg.pinv(sigma)
	lds = np.log(np.linalg.det(sigma))
	lp = np.log(p)
	sm = invSigma @ mu
	msm = mu.transpose(0, 2, 1) @ sm

	res = []
	ds = [d1, d2]
	for i in range(K):
		for j in range(i+1, K):
			# X = A x + B y + C
			dmu = mu[j] - mu[i]
			dmu /= np.linalg.norm(dmu[ds])
			A = dmu * dmu[d1]
			A[ds,0] = [1, 0]
			B = dmu * dmu[d2]
			B[ds,0] = [0, 1]
			C = mu[i] - dmu * dmu[ds].T @ mu[i,ds]
			C[ds,0] = [0, 0]
			# X.T Q X + W.T X + E = 0
			Q = invSigma[i] - invSigma[j]
			W = 2 * (sm[j] - sm[i])
			E = 2 * (lp[j] - lp[i]) + lds[i] - lds[j] + msm[i] - msm[j]
			# ===> (A.T Q A) x^2 + (2 A.T Q B) xy + (B.T Q B) y^2
			# 		+ [(2 Q C + W).T A] x + [(2 Q C + W).T B] y + [(Q C + W).T C + E] = 0
			# a x^2 + b xy + c y^2 + d x + e y + f = 0
			a = A.T @ Q @ A
			b = 2 * A.T @ Q @ B
			c = B.T @ Q @ B
			d = (2 * Q @ C + W).T @ A
			e = (2 * Q @ C + W).T @ B
			f = (Q @ C + W).T @ C + E
			coeffs = list(map(float, [a, b, c, d, e, f]))
			res += [(i, j, l) for l in plot_conic(coeffs, x0, x1, sx)]
	return res

def show_clustering(X, Ks, clus, draw_sep=False):
	"""
	Show results for different clustering methods
	X is the array containing the data
	Ks is a list of number of clusters
	clus is a list indexed by k in Ks
	clus[k] is a list of results of different models
	clus[k][i] is a tuple (model_name, y, theta)
	where model_name is a string representing the name of the model
	y is an array containing the associated cluster of each point of X
	theta is the parameters of the model (mu for kmean or (p, mu, sigma) for EM)
	"""
	d = X.shape[1]
	num_models = max(len(c) for c in clus)
	cs = pl.cm.tab10(np.linspace(0, 1, max(Ks)))[:,:3]
	for i in range(d):
		x = X[:, i]
		x0, x1 = min(x), max(x)
		dx = (x1-x0) * 0.05
		x0, x1 = x0-dx, x1+dx
		for j in range(i):
			fig, axs = pl.subplots(len(Ks), num_models, figsize=(4.4*num_models, 4.4*len(Ks)))
			if len(Ks) == 1: axs = axs[np.newaxis,:]
			pl.subplots_adjust(0.05, 0.05, 0.98, 0.97, 0.185, 0.28)
			for K in range(len(Ks)):
				for m in range(len(clus[K])):
					model_name, y, theta = clus[K][m]
					p = None
					if type(theta) == tuple: p, mu, sigma = theta
					z = X[:, j]
					z0, z1 = min(z), max(z)
					dz = (z1-z0) * 0.05
					z0, z1 = z0-dz, z1+dz
					for k in range(Ks[K]):
						axs[K,m].scatter(x[y == k], z[y == k], color=cs[k])
						if p is not None:
							axs[K,m].scatter(mu[k,i], mu[k,j], color=0.7*cs[k], marker='x')
							w, v = np.linalg.eigh(sigma[k][np.ix_([i,j],[i,j])])
							for mul in [1.2, 2, 3]:
								angle = np.math.atan2(v[1, 0], v[0, 0])
								el = Ellipse(mu[k,[i,j]], mul*w[0]**0.5, mul*w[1]**0.5, 180/np.pi*angle, facecolor='none',
												edgecolor=0.7*cs[k], linestyle='--', linewidth=0.35+0.7*K*p[k])
								axs[K,m].add_patch(el)
					if draw_sep:
						if p is None:
							v = Voronoi(list(theta[:,[i,j]]) + [[2*x0-x1, 2*z0-z1], [2*x1-x0, 2*z0-z1]])
							voronoi_plot_2d(v, ax=axs[K,m])
						else:
							sep = plot_sep(theta, x0, x1, i, j)
							for k, l, (xs, ys) in sep:
								axs[K,m].plot(xs, ys, color=cs[[k,l]].mean(0))
					axs[K,m].set_xlim(x0, x1)
					axs[K,m].set_ylim(z0, z1)
					axs[K,m].set_title(f"{model_name} ({Ks[K]} clusters)")
					axs[K,m].set_xlabel(f"dim {i}")
					axs[K,m].set_ylabel(f"dim {j}")
	pl.show()