from sklearn import datasets
import numpy as np
from drawing import *

iris = datasets.load_iris()
X = iris.data

def centerInitialization(X, K):
	"""
	Returns K points of the array X using the initialization of Kmeans++
	"""
	mu = [X[np.random.randint(X.shape[0])]]
	ds = np.linalg.norm(X - mu[0], axis=1)
	for _ in range(K-1):
		r = np.random.rand() * ds.sum()
		i = 0
		while r > ds[i]:
			r -= ds[i]
			i += 1
		mu.append(X[i])
		ds = np.minimum(ds, np.linalg.norm(X - mu[-1], axis=1))
	return np.array(mu)

def kmean_predict(mu, X):
	"""
	Given an array of centroids mu and an array of n points X,
	returns an array y of length n where y[i] is the index k of
	the closest centroids to x (i.e. y[i] = argmin_k d(x[i], mu[k])).
	"""
	return np.argmin(np.linalg.norm(np.expand_dims(X, 1) - mu, axis=2), axis=1)

def kmean(X, K, nSteps=1000):
	"""
	Apply K-means of the array of points X using at most nSteps.
	Returns an array of centroids mu
	"""
	assert K > 0
	mu = centerInitialization(X, K)
	y = kmean_predict(mu, X)
	for i in range(nSteps):
		mu = np.array([X[y == k].mean(0) for k in range(K)])
		y2 = kmean_predict(mu, X)
		if np.count_nonzero(y2 - y) == 0: break
		y = y2
	return mu

def Estep(Xc, p, mu, sigma):
	"""
		Return tau_{i, k} = P(x_i, Z_i = k | p, mu, sigma)
		The parameter Xc has shape (n, K, d, 1) and is defined by
		Xc[i][k][j][0] = x_{ij} - mu_{kj}
	"""
	n, K, d, _ = Xc.shape
	invSigma = np.linalg.pinv(sigma)
	detSigma = np.linalg.det(sigma)
	cov = Xc.transpose((0, 1, 3, 2)) @ invSigma.reshape(1, K, d, d) @ Xc
	tau = np.exp(-0.5 * cov.reshape(n, K)) * p / (detSigma * (2*np.pi)**d + 1e-10) ** 0.5
	return tau

def EM(X, K, nSteps, precision=1e-4, verbose=True):
	"""
	Apply EM algorithm with a gaussian mixture model on an array of points X.
	- K is the number of gaussians in the model
	- nSteps is the maximum number of iterations
	- precision is a value p such that the algorithm stops when the difference of
	  log-likelihood between two iterations is less than n*p when n is then length of X
	- verbose is a boolean. If True, the log-likelihood is printed at each iteration.
	--------------
	Returns a tuple of size 3 (p, mu, sigma), the parameters of the learnt model.
	These parameters are numpy arrays of shapes (K,), (K, d) and (K, d, d) where d is
	the dimension of the space.
	"""
	assert K > 0
	n, d = X.shape
	# Covariance of X
	cX = X - X.mean(0)
	sX = cX.T @ cX / n
	# Initial parameters
	p = np.ones(K) / K
	mu = centerInitialization(X, K)
	sigma = np.tile(sX.reshape(1, d, d) / K**2, (K, 1, 1)) 
	# Function to center X with respect to mu
	centered = lambda: X.reshape(n, 1, d, 1) - mu.reshape(1, K, d, 1)
	Xc = centered()
	ll = -float('inf')
	for t in range(nSteps+1):
		# E step
		# Here tau[i,k] = P(x_i, Z_i = k | theta)
		tau = Estep(Xc, p, mu, sigma)
		# L[i] = P(x_i | theta)
		L = tau.sum(1)
		# nll = log-likelihood
		nll = np.log(L).sum()
		if verbose: print("log-likelihood:", nll)
		# stopping criteria
		if nll - ll < precision*n: break
		ll = nll
		if t == nSteps: break
		# Normalization of tau
		tau /= L.reshape(n, 1)

		# M step
		p = tau.mean(0) + 1e-10
		p /= p.sum()
		mu = (tau.reshape(n, K, 1) * X.reshape(n, 1, d)).mean(0) / p.reshape(K, 1)
		Xc = centered()
		sigma = (tau.reshape(n, K, 1, 1) * (Xc @ Xc.transpose((0, 1, 3, 2)))).mean(0) / p.reshape(K, 1 ,1)
	return p, mu, sigma

def EM_diag(X, K, nSteps, precision=1e-4, verbose=True):
	"""
	Apply EM algorithm with a diagonal gaussian mixture model on an array of points X.
	- K is the number of gaussians in the model
	- nSteps is the maximum number of iterations
	- precision is a value p such that the algorithm stops when the difference of
	  log-likelihood between two iterations is less than n*p when n is then length of X
	- verbose is a boolean. If True, the log-likelihood is printed at each iteration.
	Returns a tuple of size 3 (p, mu, sigma), the parameters of the learnt model.
	These parameters are numpy arrays of shapes (K,), (K, d) and (K, d, d) where d is
	the dimension of the space. Furthermore sigma[k] is diagonal.
	"""
	assert K > 0
	n, d = X.shape
	# Initial parameters
	p = np.ones(K) / K
	mu = centerInitialization(X, K)
	# mu = np.zeros((K, d))
	D = np.tile(((X - X.mean(0)) ** 2).mean(0), (K, 1))
	# Function to center X with respect to mu
	centered = lambda: X.reshape(n, 1, d) - mu
	Xc = centered()
	ll = -float('inf')
	for t in range(nSteps+1):
		# E step
		# Here tau[i,k] = P(x_i, Z_i = k | theta)
		tau = np.exp(-0.5 * (Xc**2 / D).sum(2)) * p / (D.prod(1) * (2*np.pi)**d + 1e-10) ** 0.5
		# L[i] = P(x_i | theta)
		L = tau.sum(1)
		# nll = log-likelihood
		nll = np.log(L).sum()
		if verbose: print("log-likelihood:", nll)
		# stopping criteria
		if nll - ll < precision*n: break
		if t == nSteps: break
		ll = nll
		# Normalization of tau
		tau /= L.reshape(n, 1)

		# M step
		p = tau.mean(0) + 1e-10
		p /= p.sum()
		mu = (tau.reshape(n, K, 1) * X.reshape(n, 1, d)).mean(0) / p.reshape(K, 1)
		Xc = centered()
		D = (tau.reshape(n, K, 1) * Xc**2).mean(0) / p.reshape(K, 1)
	return p, mu, np.apply_along_axis(np.diag, 1, D)

def EMpredict(X, p, mu, sigma):
	"""
	Return the prediction of the gaussian model (p, mu, sigma) on the array X of length n
	It returns an array y of length n where y[i] is the cluster to which belongs X[i].
	"""
	n, d = X.shape
	K = p.shape[0]
	Xc = X.reshape(n, 1, d, 1) - mu.reshape(1, K, d, 1)
	tau = Estep(Xc, p, mu, sigma)
	return np.argmax(tau, axis=1)

# angle = np.linspace(0, 2*np.pi, 75, False)[:,np.newaxis]
# X0 = 4.5 * np.concatenate((np.cos(angle), np.sin(angle)), axis=1)
# n1 = 200
# direction = np.random.standard_normal((n1, 2))
# X1 = direction * (np.random.rand(n1) / np.linalg.norm(direction, axis=1))[:,np.newaxis] * [1.5, 0.5] + [1, 0.1]
# X = np.concatenate((X0, X1), 0)

Ks = [2]
clus = []
for K in Ks:
	ck = []
	mu = kmean(X, K)
	y = kmean_predict(mu, X)
	ck.append(("Kmean", y, mu))

	theta = EM(X, K, 100, verbose=False)
	p, mu, sigma = theta
	y = EMpredict(X, p, mu, sigma)
	ck.append(("EM full", y, theta))

	theta = EM_diag(X, K, 100, verbose=False)
	p, mu, sigma = theta
	y = EMpredict(X, p, mu, sigma)
	ck.append(("EM diag", y, theta))
	clus.append(ck)

show_clustering(X, Ks, clus, True)