import numpy as np
import pylab as pl

def sum_of_log(a, axis=0):
	"""
	return log(exp(a).sum(axis)) in a stable way
	"""
	m = a.max(axis)
	return m + np.log(np.exp(a-np.expand_dims(m, axis)).sum(axis))

def sum_product(psi, psi2):
	"""
	Sum produt algorithm on a chain with psi and psi2 as described in the report
	return two lists de_mes and as_mes of length n (=len(psi)) where
	de_mes[i] is the log-message from i+1 to i represented as an array of shape (len(psi[i]),)
		and de_mes[-1][a] = 0
	as_mes[i] is the log-message from i-1 to i represented as an array of shape (len(psi[i]),)
		and as_mes[0][a] = 0
	"""
	n = len(psi)
	de_mes = [np.zeros(psi[-1].shape[0])]
	as_mes = [np.zeros(psi[0].shape[0])]
	for i in range(n-1):
		de_mes.append(sum_of_log(psi[-(i+1)] + psi2[-(i+1)] + de_mes[-1], 1))
		as_mes.append(sum_of_log(np.expand_dims(psi[i] + as_mes[-1], 1) + psi2[i], 0))
	return list(reversed(de_mes)), as_mes

def marginal(psi, de_mes, as_mes):
	"""
	Compute and return the log-marginals in a list p of size n (=len(psi))
	where p[i] is an array of shape (len(psi[i]),) and p[i][a] is:
		log P(X_i = a)
	"""
	n = len(psi)
	p = [as_mes[i] + psi[i] + de_mes[i] for i in range(n)]
	return p

# def brute_force(psi, psi2, i, k):
# 	n = len(psi)
# 	psi = [np.exp(p) for p in psi]
# 	psi2 = [np.exp(p) for p in psi2]
# 	def aux(j, l):
# 		if j == i and l != k: return []
# 		if j == n-1: return [psi[j][l]]
# 		res = []
# 		for l2 in range(len(psi[j+1])):
# 			c = psi[j][l] * psi2[j][l][l2]
# 			res += [c * a for a in aux(j+1, l2)]
# 		return res
# 	L = []
# 	for l in range(len(psi[0])):
# 		L += aux(0, l)
# 	while len(L) > 1:
# 		L2 = []
# 		for i in range(len(L)//2):
# 			L2.append(L[2*i]+L[2*i+1])
# 		if len(L) % 2 == 1: L2[-1] += L[-1]
# 		L = L2
# 	return np.log(L[0])

# n = 10
# K = np.random.randint(2, 5, n)
# psi = [np.random.rand(k) for k in K]
# psi = [np.log(p / p.sum()) for p in psi]
# psi2 = [np.random.rand(K[i], K[i+1]) for i in range(n-1)]
# psi2 = [np.log(p / p.sum() * 0.5 * sum(p.shape)) for p in psi2]

# p = sum_product(psi, psi2)
# print(p)
# print(brute_force(psi, psi2, 0, 0))
# print(brute_force(psi, psi2, 1, 1))
# print(brute_force(psi, psi2, 2, 1))

# exit(0)

def Zissing(w, h, alpha, beta):
	"""
	Compute and return exact value log Z(alpha, beta) for a grid of size h*w
	beta can be a float or a list of float. In this case the result is a list.
	This function uses the junction tree and the sum product algorithm
	"""
	tb = type(beta)
	t = [list, np.ndarray]
	if tb not in t: beta = [beta]
	k = 2**w
	n_ones = [0]
	n_shift = [0, 1]
	for _ in range(w):
		n_ones += [n+1 for n in n_ones]
	for i in range(w-1):
		n_shift += n_shift
		for j in range(2**(i+1), 2**(i+1)+2**i):
			n_shift[j] += 2
	for i in range(k // 2, k):
		n_shift[i] -= 1
	n_ones = np.array(n_ones)
	n_shift = np.array(n_shift)
	psi = [[alpha * n_ones + b * (w - 1 - n_shift)] * h for b in beta]
	ind = np.arange(k)
	common = w - n_ones[ind.reshape(k, 1) ^ ind]
	psi2 = [[b * common] * (h-1) for b in beta]
	mes = [sum_product(p, p2) for p, p2 in zip(psi, psi2)]
	res = [sum_of_log(marginal(p, d, a)[0]) for p, (d, a) in zip(psi, mes)]
	if tb not in t: res = res[0]
	return res

def brute2(w, h, beta):
	"""
	Compute log Z(0, beta) for a grid of size h*w using a brute force way.
	"""
	X = [[0] * h for _ in range(w)]
	def aux(i, j):
		global Z
		if i == -1:
			n = 0
			for k in range(w):
				for l in range(h):
					if k+1 < w and X[k][l] == X[k+1][l]: n += 1
					if l+1 < h and X[k][l] == X[k][l+1]: n += 1
			return np.exp(n*beta)
		if j == -1:
			return aux(i-1, h-1)
		res = 0
		for v in [0, 1]:
			X[i][j] = v
			res += aux(i, j-1)
		return res
	return np.log(aux(w-1, h-1))

def loopy_belief(G, psi, psi2, nsteps):
	"""
	Compute messages thanks to the loopy belief propagation on the graph G
	with potentials psi and psi2 using nsteps steps.
	- G is a list of n arrays where n is the number of nodes in the graph.
	G[i] is a 1D array containing all neighboors of i
	- psi is an array of shape (n, K) where K is the number of states of each node
	psi[i, a] is the log-potential of the node i at state a (i.e. log psi_i(a))
	- psi2 is a list of n arrays representing the log-potentials of edges.
	psi2[i] as shape (len(G[i]), K, K) and psi2[i][j,a,b] represents the log-potential
	between i at state a and G[i][j] at state b (i.e. log psi_{i, G[i][j]}(a, b))
	- nsteps is an integer reprensenting the number of iterations
	-----------------
	Return a list of log-messages mes of lenght n where
	mes[i] is an array of size (len(G[i]), K) and
	mes[i][j,a] is the log-message from j to i at state a (i.e. log mu_{j -> i}(a))
	"""
	n = len(G)
	K = len(psi[0])
	# number of neigboors of each node
	nn = [len(G[a]) for a in range(n)]
	# mes[i][j,a] is log mu_{G[i][j], i}(a)
	mes = [np.zeros((nn[a], K)) for a in range(n)]
	# we transpose psi2 such that psi2[i][a][j][b] is psi_{i, G[i][j]}(a, b)
	psi2 = [p.transpose(1, 0, 2) for p in psi2]
	# invG[i][j] is the index of i in G[j]
	invG = [{G[a][i]:i for i in range(nn[a])} for a in range(n)]
	invG = [[invG[b][a] for b in G[a]] for a in range(n)]
	for s in range(nsteps):
		# b[i,a] is the log of the product of the messages arriving at i at state a (mu_{j -> i}(a)) times psi_i(a)
		# it is called the belief
		b = np.array([psi[i] + mes[i].sum(0) for i in range(n)])
		# selfmes[i][j,a] is log mu_{i, G[i][j]}(a)
		selfmes = [np.array([mes[G[i][j]][invG[i][j]] for j in range(nn[i])]) for i in range(n)]
		for i in range(n):
			mes[i] = sum_of_log(psi2[i] + b[G[i]] - selfmes[i], 2).T
	return mes

def marginalG(psi, mes, i):
	"""
	Return the log-marginal p of i where p[a] = log [Z p(X_i = a)]
	"""
	return psi[i] + mes[i].sum(0)

def getZ(psi, mes):
	"""
	Compute and return Z. See the function loopy_belief to know what are
	the parameters psi and mes
	"""
	n = len(psi)
	margs = np.array([marginalG(psi, mes, i) for i in range(n)])
	Zs = sum_of_log(margs, 1)
	Z = sum_of_log(Zs) - np.log(n)
	return Z

def Zissing2(w, h, alpha, beta):
	"""
	Same as Zissing but using the loopy belief propagation
	"""
	n = w*h
	nsteps = w+h-2
	G = []
	for i in range(n):
		g = []
		if i % w > 0: g.append(i-1)
		if (i+1) % w > 0: g.append(i+1)
		if i >= w: g.append(i-w)
		if i+w < n: g.append(i+w)
		G.append(np.array(g))
	psi = np.array([[0, alpha]]).repeat(n, 0)
	psi2 = [np.array([[[beta, 0], [0, beta]]]).repeat(len(G[i]), 0) for i in range(n)]
	mes = loopy_belief(G, psi, psi2, nsteps)
	return getZ(psi, mes)

w, h = 1, 5
alpha = 0
beta = np.linspace(-2, 1, 4)
Z = Zissing(w, h, alpha, beta)
print("Z", Z)
# pl.plot(beta, Z)
# pl.show()
Z2 = [Zissing2(w, h, alpha, b) for b in beta]
# Z2 = [brute2(w, h, b) for b in beta]
print("Z2", Z2)