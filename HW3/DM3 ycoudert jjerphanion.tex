\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\graphicspath{{images/}}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\sgn}{sgn}
\DeclareMathOperator*{\erf}{erf}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\V}{\mathbb{V}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Modèles graphiques probabiliste --- DM 3
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont, Julien Jerphanion}

\begin{document}
	
	\maketitle
	
	\section*{Gibbs sampling and mean field VB for the probit model}
	
	\paragraph{1.}
	
	Il est adapté de pré-traiter les données comme proposé pour les raisons suivantes :
	\begin{itemize}
		\item L'ajout du biais permet de gagner un degré de liberté sur l'hyperplan de la séparation définie par $\beta$ et ainsi avoir une séparation affine plutôt que linéaire.
		\item Le centrage selon chaque coordonné permet de ne pas avoir une ordonné à l'origine (la valeur de $\beta$ selon la nouvelle coordonnée constante) corrélée aux autres coordonnées de $\beta$. En effet notre \emph{a priori} sur $\beta$ est isotrope. On veut donc éviter les corrélations. En posant $\mu_x = \E[x]$ on a, à $\beta$ fixé, $\E[z] = \E[\beta^\trans x] = \beta^\trans \mu_x$. Ainsi si $\beta^\trans \mu_x$ est strictement positif les données auront plus tendance à être classé dans le cluster 1 et si cette valeur est strictement négative, les données auront plus tendances à être classé dans le cluster -1. Dans le premier cas il faudra compenser avec un biais négatif et dans le second cas avec un biais positif. D'où la corrélation entre la direction de $\beta$ et le biais. Il nous faut alors $\mu_x = 0$ et donc centrer les données.
		\item La normalisation pour avoir une variance de 1 selon chaque coordonnée permet aussi d'obtenir un \emph{a priori} plus fort sur l'isotropie de $\beta$. En effet si la variance des $x$ est plus faible selon l'axe $j$ alors pour $\beta = e_j$, le vecteur nul avec un 1 en coordonnée $j$, la variance de $\beta^\trans x = x_j$ est faible et le bruit $\epsilon$ prend le dessus sur le "signal". $\beta$ doit donc avoir une norme plus élevée selon cette direction pour palier à ce problème. Ainsi, si on ne normalise par les données notre \emph{a priori} sur $\beta$ est anisotrope, puisque la norme de $\beta$ doit être plus ou moins grande selon la direction de $\beta$ dépendant de la variance des $x_i$ selon cette même direction. C'est pourquoi on normalise. Dans l'idéal il faudrait passer nos données dans une base de coordonnées indépendantes (orthogonales) en utilisant par exemple ACP avant de normaliser.
	\end{itemize}

	De même, les colonnes dépendant d'unités différentes, la normalisation permet aussi 
	de se ramener dans un espace de même unité, ce qui fait plus sens lorsqu'il s'agit
	de calculer des distances. Enfin la normalisation permet d'améliorer le conditionnement
	de matrices utilisées pour l'estimation de $\beta$, notamment de la matrice $X^\trans X$
	définie dans la suite.

	\paragraph{2.}

	Cette variance peut être fixée à $1$ de manière canonique.
	En effet, supposons que dans un second modèle, le bruit soit gaussien pour une certaine variance $\sigma^2_2$:
	$$ \forall i \in [\![ n]\!], \quad \epsilon_i \sim \mathcal{N}(0,\sigma_2^2) $$
	On a alors:
	$$ \forall i \in [\![ n]\!], \quad \frac{\epsilon_i}{\sigma_2} \sim \mathcal{N}(0,1) $$
	et ainsi:
	$$ \forall i \in [\![ n]\!], \quad y_i = \text{sign}\left(\beta^\trans x_i + \frac{\epsilon_i}{\sigma_2}\right) $$
	ce qui donne:
	$$ \forall i \in [\![ n]\!], \quad y_i = \text{sign}\left((\beta\sigma_2)^\trans x_i + \epsilon_i\right) $$
	Ainsi, si on suppose que le bruit a pour variance $\sigma_2^2$ et qu'on l'on cherche la distribution de $\beta_2$ avec ce nouveau modèle, il nous suffit d'appliquer notre algorithme qui suppose que la variance du bruit vaut 1 afin d'obtenir $\beta$ et on dispose de la relation :
	$$ \beta_2 = \beta \sigma_2 $$
	Finalement il nous faut ajuster notre \emph{a priori} sur $\beta$. Si $\beta \sim \mathcal{N}(0, \tau I_p)$ et $\beta_2 \sim \mathcal{N}(0, \tau_2 I_p)$, alors on doit avoir :
	$$ \tau_2 = \sigma_2^2 \tau $$
	On peut donc traiter le cas où la variance de bruit vaut $\sigma_2^2$ quelconque avec le modèle présenté.
	
	\paragraph{3.}
	On commence par dessiner le graphe de notre modèle :
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}[node distance=1.3cm, every node/.style={circle, draw, minimum size=20pt}]
		\node[fill=gray!25] (y) at (0, 0) {$y_i$};
		\node[above of=y] (z) {$z_i$};
		\node[left of=z] (beta) {$\beta$};
		\node[right of=z] (eps) {$\epsilon_i$};
		\draw[->, thick] (beta) edge (z) (eps) edge (z) (z) edge (y);
		\end{tikzpicture}
	\end{figure}
	
	On s'intéresse d'abord à l'échantillonnage de $\beta$. En utilisant la formule de Bayes et l'indépendance des $(z_i)_{i=1}^n$, on obtient :
	$$ p(\beta \mid z, y) =\frac{p(\beta, z)}{p(z)} \propto p(\beta) p(z \mid \beta) \propto p(\beta) \prod_{i=1}^n p(z_i \mid \beta) \propto p(\beta) \prod_{i=1}^n p \left( \epsilon_i = z_i - \beta^\trans x_i \right) $$
	On connaît la loi \emph{a priori} de $\beta$ et la loi des $\epsilon_i$. Cela nous donne :
	$$ p(\beta \mid z,y) \propto \exp \left(-\frac{1}{2\tau} \Vert  \beta \Vert^2 - \frac{1}{2} \sum_{i=1}^n \left( z_i - \beta^\trans x_i \right)^2 \right) $$
	On reconnaît la densité d'une loi gaussienne $\mathcal{N}(\mu, \Sigma)$.
	Pour simplifier les calculs, on introduit la matrice et le vecteur suivants :
	$$ X = \pmat{\, x_1^\trans \, \\ \vdots \\ x_n^\trans} \in \R^{n\times p} \qquad y = \pmat{\, y_1 \, \\ \vdots \\ y_n} \in \R^n $$
	Cela nous permet d'écrire :
	$$ \sum_{i=1}^n \left( z_i - \beta^\trans x_i \right)^2 = \left\| z - X\beta \right\|^2 = \| z \|^2 - 2 \beta^\trans X^\trans z + \beta^\trans X^\trans X \beta $$
	On obtient alors l'expression suivante :
	$$ p(\beta \mid z,y) \propto \exp \left[ -\frac{1}{2} \left( \beta^\trans \left( \tau^{-1} I_p + X^\trans X \right) \beta - 2 \beta^\trans X^\trans z \right)  \right] $$
	De plus, on sait que la densité $q$ de $\mathcal{N}\left(\mu, \Sigma \right)$ s'écrit comme:
	$$ q(x) \propto \exp \left[ -\frac{1}{2} \left( x^\trans \Sigma^{-1} x - 2 x^\trans \Sigma^{-1} \mu \right)  \right] $$
	On peut alors identifier les paramètres de notre loi gaussienne :
	$$ \syst{lll}{
		\Sigma^{-1} & = & \tau^{-1} I_p + X^\trans X \\
		\Sigma^{-1} \mu & = & X^\trans z
	} $$
	Ce qui nous donne finalement :
	\begin{center}
		\fbox{$ \displaystyle \beta\  |\ z, y \sim \mathcal{N}(\mu, \Sigma) \qquad \text{avec}\qquad \Sigma = \left(\tau^{-1} I_p + X^\trans X\right)^{-1} \qquad \mu = \Sigma X^\trans z $}
	\end{center}

	Désormais on cherche à échantillonner $z$. Par indépendance on échantillonne les $z_i$ séparément. On utilise à nouveau la formule de Bayes :
	$$\forall i \in [\![n]\!], \quad p(z_i \mid \beta, y) = \frac{p(z_i, y_i \mid \beta)}{p(y_i \mid \beta)} \propto p(z_i \mid \beta) p(y_i \mid z_i) \propto \exp \left( - \dfrac{1}{2} \left( z_i - \beta^\trans x_i \right)^2 \right) \mathbbm{1} \{ y_i = \sgn(z_i) \} $$
	\begin{center}
		\fbox{Ainsi $z_i\ |\ \beta, y$ suit une loi normale réduite de centre $\beta^\trans x_i$ tronquée à $\R_+$ si $y_i = 1$ et à $\R_-$ sinon.}
	\end{center}

	Les densités de probabilités postérieures marginales approximées apparaissent sur les figures \ref{dens} et \ref{zs}.
	\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{./figs/dens}
	\caption{Densités de probabilités postérieures marginales des composantes de $\beta$ ; Orange : Gaussienne parametrées, Vert : Éstimation par noyau}
	\label{dens}
	\end{figure}
	\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{./figs/zs}
	\caption{Densités de probabilités postérieures marginales de certains $(z_i)_i$}
	\label{zs}
	\end{figure}

	
	\paragraph{4.}
	Dans un algorithme variationnel, on cherche à approcher une distribution $\pi$ par une distribution $q$. Ce problème peut être reformulé comme le problème de minimisation suivant~:
	$$ q = \argmin_{q \in \mathcal Q} d(q,\pi) $$
	Où $\mathcal{Q}$ est une famille de distributions, et $d$ est une distance ou une divergence définie sur l'ensemble des distributions. \\

	Généralement, $d$ est la divergence de Kullback Leibler définie par :
	$$ KL(q\|\pi) = \E_q \left[ \log \dfrac{q(x)}{\pi(x)} \right] = \int q(x) \log \left( \dfrac{q(x)}{\pi(x)} \right) dx $$
	Dans le cas de l'algorithme variationnel du champ moyen, $\mathcal{Q}$ est la famille des distributions qui se factorisent :
	$$ \mathcal{Q} = \left\{ q \mid q(x) = \prod_{i=1}^n q_i(x_i) \right\} $$
	Dans notre problème probit, on souhaite approcher la distribution suivante :
	$$ \pi(\beta, z) = p(\beta, z |y) = \frac{p(\beta, z, y)}{p(y)} = \frac{p(\beta)p(z \mid \beta)p(y \mid z)}{p(y)} $$
	définie pour $\beta \in \R^p$ et $z_i \in \R$ tel que $\sgn(z_i) = y_i$ pour $i=1,...,n$. On écrit alors notre approximation sous la forme suivante :
	$$ q(\beta,z) = q_1(\beta) q_2(z) $$
	On prendra $q_1$ une loi gaussienne paramétrisée par une espérance $\mu$ et une matrice de covariance $\Sigma$.
	$$ q_1: \quad \mathcal{N}(\mu, \Sigma) $$
	On décidera par la suite de la paramétrisation de $q_2$. \\
	
	On écrit ensuite la divergence KL :
	$$ KL(q \| \pi) = \E_q \left[ \log \frac{q(\beta, z)}{\pi(\beta,z)} \right] = \E_q \left[ \log \left( \frac{p(y) q_1(\beta) q_2(z)}{p(\beta) p(z \mid \beta) p(y \mid z)} \right) \right] $$
	On remarque ensuite que toute quantité dépendant de $y$ est constante et on remplace les probabilités connues :
	\begin{equation}
	KL(q \| \pi) = \E_q \left[ \dfrac{1}{2 \tau} \| \beta \|^2 + \dfrac{1}{2} \| z - X\beta \|^2 + \log q_1(\beta) + \log q_2(z) \right] + c^{te}
	\label{kl}
	\end{equation}
	On procède itérativement en minimisant par rapport à $q_1$, $q_2$ étant fixé, puis en minimisant par rapport à $q_2$, avec $q_1$ fixé. \\
	
	Commençons par minimiser selon $q_2$. On ne garde que les termes dépendant de $q_2$ dans \eqref{kl} :
	$$ q_2 = \argmin_{q_2} \E_{q_2} \left[ \dfrac{1}{2} \E_{q_1} \left[ \| z - X\beta \|^2 \right] + \log q_2(z) \right] $$
	On réécrit cette expression sous la forme d'une divergence :
	$$ q_2 = \argmin_{q_2} KL \left( q_2 ~\Big\|~ \exp \left( - \dfrac{1}{2} \E_{q_1} \left[ \| z - X\beta \|^2 \right] \right) \right) $$
	D'où:
	$$q_2(z) \propto \exp \left( - \dfrac{1}{2} \E_{q_1} \left[ \| z - X\beta \|^2 \right] \right)$$
	Il nous faut calculer l'espérance qui intervient :
	$$ \E_{q_1} \left[ \| z - X\beta \|^2 \right] = \| z \|^2 - 2 z^\trans X \mu + c^{te} $$
	Ce qui donne:
	$$	q_2(z) \propto \exp \left( - \dfrac{1}{2} \| z \|^2 + z^\trans X \mu \right) $$
	
	En utilisant les mêmes arguments que précédemment, on reconnait alors que $q_2$ est la densité de la loi $\mathcal{N}(X \mu, I_n)$. On paramétrisera donc $q_2$ par une loi gaussienne d'espérance $m$ et de matrice de covariance, l'identité, tronquée à l'espace $\left\{ z \in \R^n \mid \sgn(z_i) = y_i, \, i = 1, ..., n \right\}$. \\
	
	On cherche maintenant à minimiser selon $q_1$. On ne garde que les termes dépendant de $q_1$ dans \eqref{kl} :
	$$ q_1 = \argmin_{q_1} \E_{q_1} \left[ \dfrac{1}{2 \tau} \| \beta \|^2 + \dfrac{1}{2} \E_{q_2} \left[ \| z - X\beta \|^2 \right] + \log q_1(\beta) \right] $$
	On réécrit cette expression sous la forme d'une divergence :
	$$ q_1 = \argmin_{q_1} KL \left( q_1 ~\Big\|~ \exp \left( - \dfrac{1}{2 \tau} \| \beta \|^2 - \dfrac{1}{2} \E_{q_2} \left[ \| z - X\beta \|^2 \right] \right) \right) $$
	Ainsi:
	$$	q_1(\beta) \propto \exp \left( - \dfrac{1}{2 \tau} \| \beta \|^2 - \dfrac{1}{2} \E_{q_2} \left[ \| z - X\beta \|^2 \right] \right) $$
	C'est-à-dire, en développant l'espérance et en ne considérant pas les facteurs constant:
	$$ q_1(\beta) \propto \exp \left( - \dfrac{1}{2 \tau} \beta^\trans \left(\tau^{-1} I_p + X^\trans X \right) \beta + z^\trans X^\trans \E_{q_2}\left[z\right]\right) $$
	On retrouve bien l'expression de la densité d'une loi gaussienne. On procède par identification~:
	$$ \syst{lll}{
		\Sigma^{-1} & = & \tau^{-1} I_p + X^\trans X \\
		\Sigma^{-1} \mu & = & X^\trans \E_{q_2}[z]
	} \qquad \Leftrightarrow \qquad \syst{lll}{
		\Sigma & = & \left( \tau^{-1} I_p + X^\trans X \right)^{-1} \\
		\mu & = & \Sigma X^\trans \E_{q_2}[z]
	} $$ \\
	
	Il nous faut désormais calculer $\E_{q_2}[z]$. Par indépendance des $(z_i)_{i=1}^n$, on peut se ramener au calcul de $\E_{q_2}[z_i]$. Supposons $y_i = 1$ :
	$$ \E_{q_2}[z_i] = \alpha_i^{-1} \int_0^{+\infty} z_i e^{-\frac{1}{2} (z_i - m_i)^2} dz_i \qquad \text{avec} \quad \alpha_i = \int_0^{+\infty} e^{-\frac{1}{2} (z_i - m_i)^2} dz_i $$
	Calculons cette espérance :
	$$ \E_{q_2}[z_i] = m_i + \alpha_i^{-1} \int_0^{+\infty} (z_i - m_i) e^{-\frac{1}{2} (z_i - m_i)^2} dz_i = m_i - \alpha_i^{-1} \left[ e^{-\frac{1}{2} (z_i - m_i)^2} \right]_0^{+\infty} = m_i + \alpha_i^{-1} e^{- \frac{1}{2} m_i^2} $$
	Enfin on calcule $\alpha_i$ :
	$$ \alpha_i = \int_{-m_i}^{+\infty} e^{-\frac{1}{2}x^2} dx = \sqrt{2} \int_{-m_i / \sqrt{2}}^{+\infty} e^{-t^2} dt = \sqrt{\frac{\pi}{2}} \left(\underbrace{\frac{2}{\sqrt{\pi}} \int_{0}^{+\infty} e^{-t^2}\ dt}_{=1} -\frac{2}{\sqrt{\pi}}  \int_{0}^{-m_i / \sqrt{2}} e^{-t^2}\ dt\right) $$
	On a la fonction erreur de Gauss qui est définie par:
	$$ \erf: x \mapsto \frac{2}{\sqrt{\pi}} \int_0^x e^{-t^2}\ dt $$
	Ce qui donne:
	$$ \alpha_i = \sqrt{\frac{\pi}{2}} \left( 1 - \erf \left(- \frac{m_i}{\sqrt{2}} \right)  \right) $$
	On obtient finalement :
	$$ \E_{q_2}[z_i] = m_i + \sqrt{\frac{2}{\pi}} \left( 1 - \erf \left(- \frac{m_i}{\sqrt{2}} \right)  \right)^{-1} e^{- \frac{1}{2} m_i^2} $$
	
	Désormais on suppose $y_i = -1$ :
	$$ \E_{q_2}[z_i] = \alpha_i^{-1} \int_{-\infty}^0 z_i e^{-\frac{1}{2} (z_i - m_i)^2} dz_i = - \alpha_i^{-1} \int_0^{+\infty} z_i e^{-\frac{1}{2} (z_i + m_i)^2} dz_i $$
	Où
	$$ \alpha_i = \int_{-\infty}^0 e^{-\frac{1}{2} (z_i - m_i)^2} dz_i = \int_0^{+\infty} e^{-\frac{1}{2} (z_i + m_i)^2} dz_i $$
	Ce qui donne :
	$$ \E_{q_2}[z_i] = m_i - \sqrt{\frac{2}{\pi}} \left( 1 - \erf \left(\frac{m_i}{\sqrt{2}} \right)  \right)^{-1} e^{- \frac{1}{2} m_i^2} $$ \\
	
	Finalement on regroupe les deux cas dans une seule formule :
	$$ \E_{q_2}[z_i] = m_i + y_i \sqrt{\frac{2}{\pi}} \left( 1 - \erf \left(- y_i \frac{m_i}{\sqrt{2}} \right)  \right)^{-1} e^{- \frac{1}{2} m_i^2} $$ \\
	
	On a donc, pour récapituler:
	\begin{center}
		\fbox{$
			\begin{cases}
			q_2 : \quad \mathcal{N}(\mu, \Sigma) \\
			\Sigma = \left(\tau^{-1} I_p + X^\trans X)\right)^{-1} \\
			\mu = \Sigma X^\trans \E_{q_2}[z] \\
			\E_{q_2}[z_i] = m_i + y_i \sqrt{\frac{2}{\pi}}\left(1 + \erf\left(-\frac{y_i m_i}{\sqrt{2}}\right)\right)^{-1} 
			\end{cases}
			$} \\[2mm]
		\fbox{$
			\begin{cases}
			q_1 : \quad \mathcal{N}(m, I_p) \quad \text{sur}\quad  \left\{ z \in \mathbb{R}^n \mid \text{sign}(z_i) = y_i \right\}\\
			m = X  \mu
			\end{cases}
			$}
	\end{center}
	$\Sigma$ étant constant, on se ramène donc à un simple algorithme itératif qui calcule alternativement $\mu$ et $m$. \\

	Pour comparer les performances des algorithmes, on utilise simplement ce que l'on appelle \emph{efficacité}, que l'on définit comme le taux d'observations correctement discriminées
	sur le jeu de données\footnote{Dans la terminologie anglaise, il s'agit de l'\emph{accuracy}.}. Ces résultats sont donnés dans le tableau \ref{resultats} avec les résultats de la régression
	linéaire pour comparaison. \\
	Dans le tableau donné on utilise un grand nombre d'itérations, bien plus que nécessaire pour obtenir la convergence. L'information donné en terme de temps d'exécution est alors proportionnel au temps d'exécution d'une itération. On remarque ainsi qu'une itération de l'échantillonneur de Gibbs prend plus de temps qu'une itération de la méthode variationnelle. En effet dans la première méthode, la génération de variables aléatoire est gourmande, surtout la génération de gaussiennes tronquées. \\
	En revanche le nombre d'itérations nécessaire pour la convergence de la méthode variationnelle est de l'ordre de quinze. Le temps d'exécution dans ce cas est de $1.2 \cdot 10^{-3}$s. L'échantillonnage de Gibbs quant à lui est bien plus lent. Il lui faut une centaine d'itérations pour converger avec un temps d'exécution de $8.8 \cdot 10^{-2}$s.

	\begin{table}[ht]
	\centering
	\begin{tabular}{||l c c||} 
	\hline
	& \textsc{Efficacité} & \textsc{Temps d'exécution (en s)} \\ [0.5ex]
	\hline\hline
	Gibbs Sampling & 0.784 & 2.5627 \\
	Mean Field & 0.784 & 0.2625 \\
	Lin.Reg. & 0.784 & 0.0087 \\ 		
	\hline\hline
	\end{tabular}
	\caption{German Credit Dataset: Comparaison des résultats (avec 1500 itérations pour les 2 premières méthodes)}
	\label{resultats}
	\end{table}

	\paragraph{5.}
	
	Dans le cas du champ moyen, la variance estimée pour $\beta$ est la matrice de covariance $\Sigma$ à $z$ fixé. Or $z$ \textit{a posteriori} est une variable aléatoire de variance non nulle. De plus, l'espérance \textit{a posteriori} de $\beta$ conditionnellement à $z$ est $\mu = \Sigma X^\trans z$, comme on a pu le montrer pour l'échantillonnage de Gibbs. On a donc en réalité la relation :
	$$ \V[\beta] = \Sigma + \V[\mu] $$
	ce qui donne :
	$$ \V[\beta] = \Sigma + \Sigma X^\trans \V[z] X \Sigma$$
	et donc par semi-definie positivité de $\Sigma X^\trans \V[z] X \Sigma$, on a:
	$$ \V[\beta] \geq \Sigma$$
	C'est pourquoi la variance retournée par l'approche du champ moyen est sous-estimée.
	
	\paragraph{6.}
	
	Dans le cas de la séparation parfaite (voir figure \ref{sep}), notre algorithme converge bien et on se retrouve avec de très bonnes discrimination
	des observations. Les résultats sont donnés dans le tableau \ref{resultats_sep}.

	\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{./figs/sep}
	\caption{Séparation complète simple sur le carré unité}
	\label{sep}
	\end{figure}

	\begin{table}[ht]
	\centering
	\begin{tabular}{||l c c||} 
	\hline
	& \textsc{Efficacité} & \textsc{Temps d'exécution (en ms)} \\ [0.5ex]
	\hline\hline
	Gibbs Sampling & 0.996 & 0.3120 \\
	Mean Field & 0.998 & 0.0354 \\
	Lin.Reg. & 0.997 & 0.0006 \\ 		
	\hline\hline
	\end{tabular}
	\caption{Séparation complète: Comparaison des résultats}
	\label{resultats_sep}
	\end{table}

	Intéressons-nous désormais à la vraisemblance :
	$$ L(\beta) = p(y \mid \beta) = \int_{\R^n} p(y, z \mid \beta) dz = \int_{\R^n} p(z \mid \beta) p(y \mid z) dz $$
	Par indépendance des $z_i$ on obtient :
	$$ L(\beta) = \prod_{i=1}^n \int_\R p(z_i \mid \beta) p(y_i \mid z_i) dz_i = \prod_{i=1}^n \int_\R \dfrac{1}{\sqrt{2 \pi}} \exp \left( - \dfrac{1}{2} \left( z_i - \beta^\trans x_i \right)^2 \right) \mathbbm{1} \{ \sgn(z_i) = y_i \} dz_i $$
	On remarque que notre intégrale est $\frac{\alpha_i}{\sqrt{2 \pi}}$. On note $l = \log L$ la log-vraisemblance. On a alors :
	$$ l(\beta) = \sum_{i=1}^n \log \alpha_i + c^{te} = \sum_{i=1}^n \log \left( 1 - \erf \left( - y_i \dfrac{\beta^\trans x_i}{\sqrt{2}} \right) \right) + c^{te} $$
	On pose alors le vecteur $w$ suivant :
	$$ w_i = y_i \frac{x_i}{\sqrt{2}} $$
	On calcule la dérivée de la fonction $\erf$ :
	$$ \dfrac{d}{dx} \erf(x) = \frac{2}{\sqrt{\pi}} e^{-x^2} $$
	Finalement $l$ est différentiable de gradient :
	$$ \nabla l(\beta) = \sum_{i=1}^n \dfrac{2}{\sqrt{\pi}} \dfrac{w_i e^{-(w_i^\trans \beta)^2}}{1 - \erf \left( - w_i^\trans \beta \right)} $$
	et de matrice Hessienne :
	$$ \nabla^2 l(\beta) = \sum_{i=1}^n \dfrac{2}{\sqrt{\pi}} \dfrac{w_i \left[ -2w_i w_i^\trans \beta e^{-(w_i^\trans \beta)^2} \cdot \left( 1 - \erf \left( - w_i^\trans \beta \right) \right) - e^{-(w_i^\trans \beta)^2} \cdot \frac{2}{\sqrt{\pi}} w_i e^{-(w_i^\trans \beta)^2} \right]^\trans}{\left( 1 - \erf \left( - w_i^\trans \beta \right) \right)^2} $$
	Soit :
	$$ \nabla^2 l(\beta) = - \sum_{i=1}^n \dfrac{2}{\sqrt{\pi}} w_i w_i^\trans e^{-(w_i^\trans \beta)^2} \dfrac{w_i \left[ 2 w_i^\trans \beta \left( 1 - \erf \left( - w_i^\trans \beta \right) \right) + \frac{2}{\sqrt{\pi}} e^{-(w_i^\trans \beta)^2} \right]}{\left( 1 - \erf \left( - w_i^\trans \beta \right) \right)^2} $$
	Or l'exponentielle est positive et comme $\erf \in ]-1, 1[$, le terme $1-\erf$ est positif. Finalement si on suppose que $\beta$ sépare parfaitement les données. Cela veut dire que $\beta^\trans x_i$ est du même signe que $y_i$ et donc que :
	$$ y_i \beta^\trans x_i \geqslant 0 \Leftrightarrow w_i^\trans \beta \geqslant 0 $$
	Dans ce cas chaque terme de la somme est semi-définie positif. On remarque ensuite la présence du moins devant la somme, qui nous donne la concavité de $l$ pour un $\beta$ qui sépare parfaitement. Finalement :
	\begin{center}
		\fbox{\parbox{\linewidth}{ \centering
			$l$ est concave sur l'ensemble des $\beta$ qui séparent parfaitement les données. \\
			Ainsi si on est capable de trouver un tel $\beta$, il est possible de calculer le maximum de vraisemblance, via la méthode de Newton par exemple.
		}}
	\end{center}

	\section*{Structure du code}
	
	Dans l'archive \verb|MVA DM3 ycoudert jjerphanion.zip| se trouvent les données dans un fichier \verb|german.data-numeric| et trois fichier Python. Dans le fichier \verb|methods.py| se trouvent entre autre nos implémentation de l'échantillonneur de Gibbs et de la méthode variationnelle. Le fichier \verb|probit.py| permet d'afficher les résultats pour le jeu de donné German et le fichier \verb|complete_separation.py| permet d'afficher les résultats pour notre jeu de donnés parfaitement séparable (c.f. \figurename~\ref{sep}).

\end{document}