import numpy as np
import time
import pylab as pl
import seaborn as sns
from sklearn.linear_model import LinearRegression
from methods import *

# Complete separation

n = 1000
n_iter = 500
adapt_data = False # Better results are obtaines without normalizing data

### Gen DATA ###
X0 = np.random.rand(n, 2)
if adapt_data: X = adapt_X(X0)
else: X = X0
beta = np.array([-1,1])
y = np.sign((X0 @ beta > 0)*2 - 1)
################

print("==== Gibbs ====")
start_gibbs = time.time()
betas, zs = gibbs(X, y, n_iter)
mean = betas.mean(0)
end_gibbs = time.time()
print("beta :", mean)
print("var :", betas.var(0))
print("Efficacité :", ((X @ mean) * y > 0).mean())
print(f"Temps d'exécution : {end_gibbs - start_gibbs} s")


print("==== Mean Field ====")
start_mf = time.time()
mu, m = mean_field(X, y, n_iter)
end_mf = time.time()
sigma = np.linalg.inv(np.eye(X.shape[1])/10**2 + X.T @ X)
print("beta :", mu)
print("var :", np.diag(sigma))
print("Efficacité :", ((X @ mu) * y > 0).mean())
print(f"Temps d'exécution : {end_mf - start_mf} s")


print("==== Lin Reg ====")
start_lr = time.time()
lr = LinearRegression(fit_intercept=False).fit(X, y)
end_lr = time.time()
print("beta :", lr.coef_ * 0.5*(np.linalg.norm(mean) + np.linalg.norm(mu)) / np.linalg.norm(lr.coef_))
p = lr.predict(X)
print("Efficacité :", (p * y > 0).mean())
print(f"Temps d'exécution : {end_lr - start_lr} s")


pl.scatter(x=X0[:,0], y=X0[:,1], c=y)
pl.plot(np.linspace(0,1), np.linspace(0,1))
ax = pl.gca()
q = ax.quiver(np.array((0.5)), np.array((0.5)), np.array((-0.1)), np.array((0.1)), units='xy', scale=1)
pl.legend(["Séparation", "Données", r"$\beta$"])
pl.show()