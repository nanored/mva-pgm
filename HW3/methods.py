import numpy as np
import pylab as pl
from scipy.special import erf
from scipy.stats import truncnorm

def adapt_X(X):
	X2 = X - X.mean(0) # Center
	X2 /= (X2**2).mean(0) ** 0.5 # n_iterormalize
	X2 = np.hstack((X2, np.ones((X.shape[0], 1)))) # add bias
	return X2

def gibbs(X, y, n_iter, tau=1e2):
	n, p = X.shape
	sigma = np.linalg.inv(np.eye(p)/tau + X.T @ X)
	M = sigma @ X.T
	betas, zs = np.zeros((n_iter, p)), np.zeros((n_iter, n))
	for i in range(n_iter):
		mu = M @ (zs[i-1] if i > 0 else y)
		betas[i] = np.random.multivariate_normal(mu, sigma)
		means = y * (X @ betas[i])
		zs[i] = y * truncnorm.rvs(a=-means, b=np.inf, loc=means)
	return betas, zs

def mean_field(X, y, n_iter, tau=1e2):
	n, p = X.shape
	sigma = np.linalg.inv(np.eye(p)/tau + X.T @ X)
	M = sigma @ X.T
	mu = np.zeros(p)
	m = np.zeros(n)
	for _ in range(n_iter):
		m = X @ mu
		mu = M @ (m + y*np.sqrt(2/np.pi)/(1 - erf(-y*m/2**0.5))*np.exp(-0.5*m**2))
	return mu, m

def approx_plot(param):
	m, v = param.mean(), param.var()
	gauss = lambda x: (2*np.pi*v)**(-0.5) * np.exp(-0.5*(x-m)**2/v)
	xs = np.linspace(param.min(), param.max(), 100)
	nb = param.shape[0] // 25
	pl.hist(param, bins=nb, density=True)
	ys = gauss(xs)
	pl.plot(xs, ys)