3. From the calculations in the last lecture, implement a Gibbs sampler for this model (based on the introduction of latent variables

$$
z_i = \beta^\top x_i + \epsilon_i
$$

as explained during the course). Run your algorithm on the German dataset, and plot the (approximated) marginal posterior distribution of each parameter.

---

We have the following graphical model:

> see `graphical_model.tex`

To recap:

- $(x_i)_{i=1}^n$ are given
- $\beta \sim \mathcal{N}(0,\tau), \tau = 10^{2}$
- $\forall i \in [\![n]\!],\quad \epsilon_i \sim \mathcal{N}(0,1)$ 
- $\forall i \in [\![n]\!], \quad (z_i = \beta^\top x_i + \epsilon_i ) \ | \ \beta\sim \mathcal{N}(\beta^\top x_i, 1)$

Using the Chain Rule, we get:
$$
p(\beta, z, y) = p(\beta)p(z|\beta) p(y|z,\beta)
$$
The structure of the graph gives:
$$
p(\beta|z,y) = p(\beta| z)
$$
Using Bayes formula and the independence of $(z_i)_{i=1}^n$, we get:
$$
p(\beta|z,y) =\frac{p(\beta, z)}{p(z)} \propto p(\beta) p(z|\beta) \propto p(\beta) \prod_{i=1}^np(z_i|\beta)
$$

$$
p(\beta|z,y) \propto \exp \left(-\frac{1}{2\tau} \Vert  \beta \Vert^2 - \frac{1}{2} \sum_{i=1}^n (z_i - \beta^\top x_i)^2\right)
$$

In order to identify, we rewrite on the second term of the argument of $\exp$ above. By considering:
$$
X = \left[\begin{matrix} — x_1^\top — \\ \dots \\ — x_n^\top —   \end{matrix}\right] \in \mathbb{R}^{n\times p}
$$
We have:
$$
\sum_{i=1}^n (z_i - \beta^\top x_i)^2 = \Vert z - X\beta\Vert^2
$$
We know the least-squares estimator $\widehat{\beta} = (X^\top X)^{-1}X^\top z$  of $\beta$ to be the orthogonal projection on a subspace of $\mathbb{R}^p$, using Pythagorea Theorem, we get:


$$
\Vert z - X\beta\Vert^2 = \Vert z - X\widehat\beta + X(\widehat{\beta} - \beta)\Vert^2 = \Vert z - X\widehat\beta \Vert^2+ \Vert X(\widehat{\beta} - \beta)\Vert^2
$$
$\Vert z - X\widehat\beta \Vert^2$ is entirely defined by data, thus we can focus on the second term:


$$
\Vert X(\widehat{\beta} - \beta)\Vert^2 = (\widehat \beta - \beta)^\top X^\top X (\widehat \beta - \beta) \propto \beta^\top X^\top X \beta
$$

We thus have a more convenient expression of $p(\beta|z, y)$:
$$
p(\beta|z,y) \propto \exp \left(-\frac{1}{2\tau} \Vert  \beta \Vert^2 - \frac{1}{2} \beta^\top X^\top X \beta \right)
$$

$$
p(\beta|z,y) \propto \exp \left(-\frac{1}{2} \beta^\top \left(\frac{1}{\tau} I_p\right) \beta - \frac{1}{2} \beta^\top X^\top X \beta \right)
$$

$$
p(\beta|z,y) \propto \exp \left(-\frac{1}{2} \beta^\top \left(\frac{1}{\tau} I_p + X^\top X\right) \beta \right)
$$

We hence have:
$$
\beta\ |\ z, y \sim \mathcal{N}(0, \Sigma) \quad \text{with}\quad \Sigma = \left(\frac{1}{\tau} I_p + X^\top X\right)^{-1}
$$

---

The Mean Field Variational Algorithm is a variationnal one.

In variational distribution, one want to approch a distribution $\pi$ by a distribution $q$. This problem can be reframed as a minimization problem formulated as:
$$
q = \text{argmin}_{q \in \mathcal Q} d(q,\pi)
$$
where $\mathcal{Q}$ is a given family of distribution, and $d$ is a distance or divergence defined on the set of distributions.

Generally, $d$ is taken as the Kullback Leibler divergence that is defined as:
$$
KL(q||\pi) = \mathbb{E}_q\left[ \log \frac{q(x)}{\pi(x)}\right] = \int q(x) \log\left(\frac{q(x)}{\pi(x)}\right) \text{d} x
$$
In the case of Mean Field Variational Algorithm, $\mathcal{Q}$ is the family of distribution that factorizes ; that is:
$$
\mathcal{Q} = \left\{q\ |\ q(x) = \prod_{i=1}^n q_i(x_i)\right\}
$$

---

In our case, we will approximate 
$$
\pi(\beta, z) = p(\beta, z |y) = \frac{p(\beta)p(z|\beta)p(y|z, \beta)}{p(y)}
$$
by:
$$
q(\beta,z) = q(\beta)q(z)
$$
We use the KL divergence, which is equal to:
$$
KL(q||\pi) = \mathbb{E}_q\left[\frac{\pi(\beta, z)}{q(\beta,z)}\right]=   \mathbb{E}_q\left[\log\left(\frac{p(\beta) p(z|\beta)p(y|z,\beta)}{p(y) q(\beta) q(z)}\right)\right]
$$

$$
KL(q||\pi) = \mathbb{E}_q\left[\log q(\beta) + \log q(z) - \log p(\beta) - \log p(z |\beta) - \log p(y|z, \beta) \right] + \log p(y)
$$

as $p(y)$ is a constant for our problem, this rewrites to
$$
\min_{q \in \mathcal Q} \mathbb{E}_q\left[\log q(\beta) + \log q(z) - \log p(\beta) - \log p(z |\beta) - \log p(y|z, \beta) \right]
$$
We proceed iteratively on minimizing with respect to $\beta \mapsto q(\beta)$, $q(z)$ being fixed and then with respect to $z\mapsto q(z)$, $q(\beta)$ being fixed. Let us fix $q(\beta)$ and optimize with respect to $z \mapsto q(z)$:

Let's now fix $q(z)$, we have:
$$
\begin{align*}
KL(q||\pi)&  \propto \mathbb{E}_q\left[\log q(\beta) - \log p(\beta) - \log p(z|\beta) - \log p(y|z,\beta) \right]& + p(y)\\
& \propto \mathbb{E}_{q(\beta)}\left[ \log q(\beta) - \log p(\beta) - \mathbb{E}_{q(z)}\left[\log(p(z|\beta) p(y|z, \beta)\right]\right] &+ p(y)\\
& \propto \mathbb{E}_{q(\beta)}\left[\log\left(\frac{q(\beta)}{p(\beta)\exp(\mathbb{E}_{q(z)}\left[\log (p(z|\beta)p(y|z, \beta))\right])}\right)\right] & + p(y)
\end{align*}
$$
which is minimized for:
$$
q(\beta) \propto p(\beta)\exp( \mathbb{E}_{q(z)}\left[ \log p(y,z|\beta)\right])
$$
We have by fixing $q(\beta)$:
$$
\begin{align*}
KL(q||\pi)&  \propto \mathbb{E}_q\left[\log q(z) - \log p(z|\beta) - \log p(y|z,\beta) \right]& + p(y)\\
& \propto \mathbb{E}_{q(z)}\left[ \log q(z) - \mathbb{E}_{q(\beta)}\left[\log(p(z|\beta) p(y|z, \beta)\right]\right] &+ p(y)\\
& \propto \mathbb{E}_{q(z)}\left[\log\left(\frac{q(z)}{\exp(\mathbb{E}_{q(\beta)}\left[\log (p(z|\beta)p(y|z, \beta))\right])}\right)\right] & + p(y)
\end{align*}
$$
which is minimized for:
$$
q(z) \propto \exp( \mathbb{E}_{q(\beta)}\left[ \log p(y,z|\beta)\right])
$$



The algorithm is given by:

**Initialisation**: Init $q^{(0)}(\beta)$, $q^{(0)}(z)$

For $t=1,\dots, T$:

- $q^{(t)}(\beta) = p(\beta)\exp\left(\mathbb{E}_{q^{(t-1)}(z)}\left[\log p(y,z|\beta)\right]\right)$
- $q^{(t)}(z) = \exp\left(\mathbb{E}_{q^{(t)}(\beta)}\left[\log p(y,z|\beta)\right]\right)$



We have:
$$
\begin{align*}
\log p(y, z|\beta) &= \log p(y|z,\beta) + \log p(z|\beta) \\
\log p(y, z|\beta) &= \underbrace{\log \left( \prod_{i=1}^n \mathbf{1}(y_i = z_i) \right)}_{=0} + \log f_{\mathcal{N}(X\beta,I_p)}(z) \\
\log p(y, z|\beta) &= -\frac{d}{2} \log 2\pi - \frac{1}{2}\sum_{i=1}^n (z_i - \beta^\top x_i)
\end{align*}
$$