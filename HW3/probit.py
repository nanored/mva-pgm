import numpy as np
import pylab as pl
import time
import seaborn as sns
from sklearn.linear_model import LinearRegression
from methods import *

# Loading data
data = np.loadtxt("./german.data-numeric")
n = data.shape[0]
X = data[:,:-1]
X = adapt_X(X)
y = data[:,-1] * 2 - 3

# Used for Gibbs and Mean Field
n_iter = 1000

print("==== Gibbs ====")
start_gibbs = time.time()
betas, zs = gibbs(X, y, n_iter)
mean = betas.mean(0)
end_gibbs = time.time()
print("beta[:5] :", mean[:5])
print("var :", betas.var(0)[:5])
print("Efficacité :", ((X @ mean) * y > 0).mean())
print(f"Temps d'exécution : {end_gibbs - start_gibbs} s")

print("==== Mean Field ====")
start_mf = time.time()
mu, m = mean_field(X, y, n_iter)
end_mf = time.time()
sigma = np.linalg.inv(np.eye(X.shape[1])/10**2 + X.T @ X)
print("beta[:5] :", mu[:5])
print("var :", np.diag(sigma)[:5])
print("Efficacité :", ((X @ mu) * y > 0).mean())
print(f"Temps d'exécution : {end_mf - start_mf} s")

print("==== Lin Reg ====")
start_lr = time.time()
lr = LinearRegression(fit_intercept=False).fit(X, y)
end_lr = time.time()
print("beta[:5] :", lr.coef_[:5] * 0.5*(np.linalg.norm(mean) + np.linalg.norm(mu)) / np.linalg.norm(lr.coef_))
p = lr.predict(X)
print("Efficacité :", (p * y > 0).mean())
print(f"Temps d'exécution : {end_lr - start_lr} s")

# Marginals of beta
fig = pl.figure()
pl.subplots_adjust(0.03, 0.05, 0.98, 0.97, 0.18, 0.38)
for i in range(betas.shape[1]):
	pl.subplot(5, 5, i+1)
	pl.title(fr"$\beta_{{{i+1}}}$")
	approx_plot(betas[:,i])
	sns.kdeplot(betas[:,i])
pl.show()

# Marginals of z
fig = pl.figure()
for k, i  in enumerate([42, 124, 11, 777]):
	pl.subplot(2, 2, k+1)
	pl.title(fr"$z_{{{i}}}$")
	nb = n_iter // 25
	pl.hist(zs[:, i], bins=nb, density=True)
pl.show()
